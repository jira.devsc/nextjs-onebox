import { listArtikel } from "@/templates/Api/PortalController";
import Footer from "@/templates/Components/Footer";
import Navbar from "@/templates/Components/Navbar";
import PartnershipTemp from "@/templates/Partnership/PartnershipTemp";
import { NextPage } from "next";
import Head from "next/head";

const Index: NextPage = (props: any) => {
  const { profil, produk, solusi, artikel } = props;
  const profileSite = { profil, produk, solusi };
  return (
    <>
      <Head>
        <title>Onebox - Menjadi Partner</title>
        <meta name="description" content="Portal Kecamatan Kota Depok" />
      </Head>
      <Navbar {...profileSite} />
      <PartnershipTemp {...props} />
      <Footer {...profileSite} />
    </>
  )
}

export default Index;

export async function getServerSideProps() {
  const res = await listArtikel();
  return {
      props: {
        profil: res.profil,
        produk: res.produk,
        solusi: res.solusi,
        artikel: res.artikel,
      }
  }
}