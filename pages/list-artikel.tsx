import { listArtikel } from "@/templates/Api/PortalController";
import ListArtikelTemp from "@/templates/ListArtikel/ListArtikelTemp";
import { NextPage } from "next";
import Head from "next/head";

const Index: NextPage = (props: any) => {
  return (
    <>
      <Head>
        <title>Onebox - Artikel</title>
      </Head>
      <ListArtikelTemp {...props} />
    </>
  )
}

export default Index;

export async function getServerSideProps() {
  const res = await listArtikel();
  return {
      props: {
        profil: res.profil,
        produk: res.produk,
        solusi: res.solusi,
        artikel: res.artikel,
      }
  }
}