import { getBoot } from "@/templates/Api/PortalController";
import Footer from "@/templates/Components/Footer";
import Navbar from "@/templates/Components/Navbar";
import { NextPage } from "next";
import Head from "next/head";

const Index: NextPage = (props: any) => {
  const { profil } = props;
  return (
    <>
      <Head>
        <title>Onebox - Privacy</title>
      </Head>
      <Navbar {...props} />

      {/* <!-- /header --> */}
<section className="atas-wrapper bg-soft-primary">
  <div className="container pt-10 pb-7 pt-md-10 pb-md-10 text-center">
    <div className="row">
      <div className="col-md-9 col-lg-7 col-xl-6 mx-auto">
        <h1 className="display-1 mb-3">Tentang Kami</h1>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb justify-content-center">
            <li className="breadcrumb-item"><a href="{{url('/')}}" title="Beranda">Beranda</a></li>
            <li className="breadcrumb-item"><a>Tentang Kami</a></li>
          </ol>
        </nav>
      </div>
      {/* <!-- /column --> */}
    </div>
    {/* <!-- /.row --> */}
  </div>
  {/* <!-- /.container --> */}
</section>

{/* <!-- /header --> */}
<section className="tentang d-lg-flex align-items-centerd-lg-flex align-items-center">
  <div className="container pt-3 pt-md-11">
    <div className="row" style={{justifyContent: 'space-between'}}>
      <div className="col-lg-6">
        <h4>Tentang Kami</h4>
        <p>Ciptadra Softindo didirikan pada tahun 1999. Tim kami lulus dari lembaga pendidikan ternama, dan
          beberapa di antaranya memiliki sertifikasi Teknologi Spesialis Microsoft. Ciptadra Softindo memiliki
          ratusan portofolio di bidang inovasi dan terbukti membantu meningkatkan produktivitas,
          pendapatan, efisiensi, memperbaiki budaya kerja, dan kepuasan pelanggan.</p>
          
          
          <p>Ciptadra Softindo hadir untuk membantu individu dan organisasi meningkatkan penjualan,
          mengubah cara beroperasi, mempermudah tugas, meningkatkan efisiensi, serta mendukung
          pengambilan keputusan yang lebih baik.</p>
          
          <p>Berkantor pusat di Depok, Jawa Barat, Indonesia dan didukung oleh sumber daya yang terus belajar
          untuk menciptakan solusi yang terintegrasi penuh dan memudahkan bisnis. Selama lebih dari 23
          tahun Ciptadra Softindo telah memiliki berbagai bidang kompetensi yang bertujuan untuk menjadi
          pilihan solusi dalam membantu bisnis berkembang.</p>

          <div className="produk-onebox">
            <h4>Produk dari Ciptadra Softindo</h4>

            <div className="row">
              <div className="col-lg-4">
                <div className="card">
                  <img src="/img/client/onebox.png" alt="portofolio" loading="lazy" />
                </div>
              </div>

              <div className="col-lg-4">
                <div className="card">
                  <img src="/img/client/orbeets.png" alt="portofolio" loading="lazy" />
                </div>
              </div>

              <div className="col-lg-4">
                <div className="card">
                  <img src="/img/client/sc.png" alt="portofolio" loading="lazy" />
                </div>
              </div>
            </div>

            <p>
              <a href="https://ciptadrasoft.com/" target="_blank">Pelajari selengkapnya tentang Ciptadra softindo<i className='bx bx-chevron-right' ></i></a>
            </p>
           
          </div>

      </div>

      <div className="col-lg-5">
        <img src="/img/illustrations/partner.svg" alt="" />
      </div>
    </div>

   
    
  </div>


</section>

<section className="card-prom wrapper" style={{background: '#F2F4F7'}}>
  <div className="container py-md-2">
    <div className="row align-items-center">
      <div className="card-pad container py-md-2">
        <div className="card-solusi">
          <h5>Onebox : Contact Center Omnichanel</h5>
          <p>Sempurnakan customer experience dalam satu platform yang terintegrasi.</p>

          <div className="justify-content-center justify-content-lg-start mt-5" data-cues="slideInDown" data-group="page-title-buttons" data-delay="200">
            <a className="feature-button" href="https://wa.me/{{$profil[0]['Telp']}}?text=Halo Tim Onebox, Saya ingin mengajukan beberapa pertanyaan" target="_blank">Hubungi Whatsapp Kami</a>
          </div>
        </div>
      </div>
    </div>
    {/* <!--/.row --> */}

    {/* <!--/.row --> */}
  </div>
</section>

      <Footer {...props} />
    </>
  )
}

export default Index;

export async function getServerSideProps() {
  const res = await getBoot();
  return {
    props: {
      profil: res.profil,
      produk: res.produk,
      solusi: res.solusi,
    }
  }
}