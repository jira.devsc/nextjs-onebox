import { detailArtikel } from "@/templates/Api/PortalController";
import Footer from "@/templates/Components/Footer";
import Navbar from "@/templates/Components/Navbar";
import DetailArtikelTemp from "@/templates/ListArtikel/DetailArtikelTemp";
import { NextPage } from "next";
import Head from "next/head";
import Script from "next/script";

const Index: NextPage = (props: any) => {
  const { profil, produk, solusi, detailArtikel, site, artikelSlide } = props;
  const profileSite = { profil, produk, solusi };

  return (
    <>
      <Head>
        <title>Onebox - Artikel</title>
      </Head>

      <Script src="/js/plugins.js" />
      <Script src="/js/theme.js" />

      <Navbar {...profileSite} />

      <DetailArtikelTemp {...props} />

      <Footer {...profileSite} />
    </>
  )
}

export default Index;

export async function getServerSideProps(context: any) {
  const { slug } = context.query;
  const res = await detailArtikel(slug);
  return {
    props: {
      profil: res.profil,
      produk: res.produk,
      solusi: res.solusi,
      detailArtikel: res.detailArtikel,
      site: res.site,
      artikelSlide: res.artikelSlide,
    }
  }
}