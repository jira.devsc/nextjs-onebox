import { listProduk } from "@/templates/Api/PortalController";
import Footer from "@/templates/Components/Footer";
import Navbar from "@/templates/Components/Navbar";
import ListProdukTemp from "@/templates/ListProduk/ListProdukTemp";
import { NextPage } from "next";
import Head from "next/head";

const Index: NextPage = (props: any) => {
  const { profil, produk, solusi, menu } = props;
  const profileSite = { profil, produk, solusi };

  return (
    <>
      <Head>
        <title>Onebox - Produk Omnichanel</title>
      </Head>
      <Navbar {...profileSite} />

      <ListProdukTemp produk={produk} />

      <Footer {...profileSite} />
    </>
  )
}

export default Index;

export async function getServerSideProps() {
  const res = await listProduk();
  return {
    props: {
      profil: res.profil,
      produk: res.produk,
      solusi: res.solusi,
    }
  }
}