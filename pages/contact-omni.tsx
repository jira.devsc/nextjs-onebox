import { getBoot } from "@/templates/Api/PortalController";
import Footer from "@/templates/Components/Footer";
import Navbar from "@/templates/Components/Navbar";
import ContactOmniTemp from "@/templates/ContactOmni/ContactOmniTemp";
import { NextPage } from "next";
import Head from "next/head";

const Index: NextPage = (props: any) => {
  return (
    <>
      <Head>
        <title>Onebox - Contact Center</title>
      </Head>
      <Navbar {...props} />
      <ContactOmniTemp />
      <Footer {...props} />
    </>
  )
}

export default Index;

export async function getServerSideProps() {
  const res = await getBoot();
  return {
      props: {
        profil: res.profil,
        produk: res.produk,
        solusi: res.solusi,
      }
  }
}