import { getBoot } from "@/templates/Api/PortalController";
import Footer from "@/templates/Components/Footer";
import Navbar from "@/templates/Components/Navbar";
import { NextPage } from "next";
import Head from "next/head";

const Index: NextPage = (props: any) => {
  const { profil } = props;
  return (
    <>
      <Head>
        <title>Onebox - Privacy</title>
      </Head>
      <Navbar {...props} />

      <section className="atas-wrapper bg-soft-primary">
    <div className="container pt-10 pb-7 pt-md-10 pb-md-10 text-center">
      <div className="row">
        <div className="col-md-9 col-lg-7 col-xl-6 mx-auto">
          <h1 className="display-1 mb-3">Whats New</h1>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb justify-content-center">
              <li className="breadcrumb-item"><a href="{{url('/')}}" title="Beranda">Beranda</a></li>
              <li className="breadcrumb-item"><a title="What's New">Whats New</a></li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>

<section>
    <div className="whats-new-page-component mb-10">
        <div className="whats-new-page-content">
            <div className="items-list-wrapper">
                <div className="square-list-container">
                    <div className="square-list">
                        <h2 className="square-list-title">Semua Update Terkini</h2>

                        <div className="desktop-date-title">
                            <div className="line-dot-container">
                                <div className="vertical-line page-first-item"></div>
                                <div className="date-dot">
                                    <div className="inner-dot"></div>
                                    <div className="outer-ring"></div>
                                </div>
                            </div>
                            <div className="date-content">Desember 2023</div>
                        </div>
                        <div className="square-wrapper">
                            <div className="line-dot-container">
                                <div className="vertical-line"></div>
                                <div className="dot"></div>
                            </div>
                            <div className="square" style={{marginBottom: '0'}}>
                                <div className="content-wrapper">
                                    <div className="card-top-part-wrapper">
                                        <h4 className="release-type-name"><i className='bx bx-tachometer' ></i>Improvement</h4>
                                        <span className="separator-dot" style={{color: '#006278'}}>•</span>
                                        <p className="full-date-title">8 Des 2023</p>
                                    </div>
                                    <div className="name"><strong>1.48.1</strong>Case Detail : penambahan text pada action Reply, Note dan Forward
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                       
                        <div className="square-wrapper">
                            <div className="line-dot-container">
                                <div className="vertical-line"></div>
                                <div className="dot"></div>
                            </div>
                            <div className="square" style={{marginBottom: '0'}}>
                                <div className="content-wrapper">
                                    <div className="card-top-part-wrapper">
                                        <h4 className="release-type-name"><i className='bx bx-tachometer' ></i>Improvement</h4>
                                        <span className="separator-dot" style={{color: '#006278'}}>•</span>
                                        <p className="full-date-title">7 Des 23</p>
                                    </div>
                                    <div className="name"><strong>1.47.3</strong>Case List : Status Solve ditambahkan di list sub menu Status - All</div>
                                  
                                </div>
                            </div>
                        </div>
                        <div className="square-wrapper">
                            <div className="line-dot-container">
                                <div className="vertical-line"></div>
                                <div className="dot"></div>
                            </div>
                            <div className="square" style={{marginBottom: '0'}}>
                                <div className="content-wrapper">
                                    <div className="card-top-part-wrapper">
                                        <h4 className="release-type-name"><i className='bx bx-tachometer' ></i>Improvement</h4>
                                        <span className="separator-dot" style={{color: '#006278'}}>•</span>
                                        <p className="full-date-title">7 Des 23</p>
                                    </div>
                                    <div className="name"><strong>1.47.3</strong>Case List : Status Solve ditambahkan di list sub menu Status - All</div>
                                   
                                </div>
                            </div>
                        </div>
                        <div className="square-wrapper">
                            <div className="line-dot-container">
                                <div className="vertical-line"></div>
                                <div className="dot"></div>
                            </div>
                            <div className="square" style={{marginBottom: '0'}}>
                                <div className="content-wrapper">
                                    <div className="card-top-part-wrapper">
                                        <h4 className="release-type-name"><i className='bx bx-tachometer' ></i>New Feature </h4>
                                        <span className="separator-dot" style={{color: '#006278'}}>•</span>
                                        <p className="full-date-title">6 Des 23</p>
                                    </div>
                                    <div className="name"><strong>1.47.0</strong>Integrasi Website Direct Login</div>
                                   
                                </div>
                            </div>
                        </div>


                        <div className="desktop-date-title">
                            <div className="line-dot-container">
                                <div className="vertical-line"></div>
                                <div className="date-dot">
                                    <div className="inner-dot"></div>
                                    <div className="outer-ring"></div>
                                </div>
                            </div>
                            <div className="date-content">November</div>
                        </div>
                        <div className="square-wrapper">
                            <div className="line-dot-container">
                                <div className="vertical-line"></div>
                                <div className="dot"></div>
                            </div>
                            <div className="square" style={{marginBottom: '0'}}>
                                <div className="content-wrapper">
                                    <div className="card-top-part-wrapper">
                                        <h4 className="release-type-name"><i className='bx bx-tachometer' ></i>Development</h4>
                                        <span className="separator-dot" style={{color: '#006278'}}>•</span>
                                        <p className="full-date-title">23 Nov 2023</p>
                                    </div>
                                    <div className="name"><strong>1.46.0</strong>Customer Portal and Repost SLA Case by Response</div>
                                </div>

                            </div>
                        </div>
                        <div className="square-wrapper">
                            <div className="line-dot-container">
                                <div className="vertical-line"></div>
                                <div className="dot"></div>
                            </div>
                            <div className="square" style={{marginBottom: '0'}}>
                                <div className="content-wrapper">
                                    <div className="card-top-part-wrapper">
                                        <h4 className="release-type-name"><i className='bx bx-tachometer' ></i>Improvement </h4>
                                        <span className="separator-dot" style={{color: '#006278'}}>•</span>
                                        <p className="full-date-title">21 Nov 2023</p>
                                    </div>
                                    <div className="name"><strong>1.45.4</strong>Case Detail: Merapikan screen contact info</div>
                                </div>

                            </div>
                        </div>
                        <div className="square-wrapper">
                            <div className="line-dot-container">
                                <div className="vertical-line"></div>
                                <div className="dot"></div>
                            </div>
                            <div className="square" style={{marginBottom: '0'}}>
                                <div className="content-wrapper">
                                    <div className="card-top-part-wrapper">
                                        <h4 className="release-type-name"><i className='bx bx-tachometer' ></i>Improvement </h4>
                                        <span className="separator-dot" style={{color: '#006278'}}>•</span>
                                        <p className="full-date-title">17 Nov 2023</p>
                                    </div>
                                    <div className="name"><strong>1.45.3</strong>Case Detail: Penyesuaian area kosong agar lebih presisi</div>
                                    {/* {{-- <div className="description">We’ve added the ability for an admin to
                                        transfer all of a user’s integrations to another user. The
                                        transferred integrations will be deactivated so that the new
                                        owner can reactivate them using their own credentials.</div> --}} */}
                                </div>

                            </div>
                        </div>
                        <div className="square-wrapper">
                            <div className="line-dot-container">
                                <div className="vertical-line"></div>
                                <div className="dot"></div>
                            </div>
                            <div className="square" style={{marginBottom: '0'}}>
                                <div className="content-wrapper">
                                    <div className="card-top-part-wrapper">
                                        <h4 className="release-type-name"><i className='bx bx-tachometer' ></i>Improvement </h4>
                                        <span className="separator-dot" style={{color: '#006278'}}>•</span>
                                        <p className="full-date-title">6 Nov 2023</p>
                                    </div>
                                    <div className="name"><strong>1.45.0</strong>Scheduler dengan Model Baru (crontab)</div>
                                    {/* {{-- <div className="description">We’ve added the ability for an admin to
                                        transfer all of a user’s integrations to another user. The
                                        transferred integrations will be deactivated so that the new
                                        owner can reactivate them using their own credentials.</div> --}} */}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

      <Footer {...props} />
    </>
  )
}

export default Index;

export async function getServerSideProps() {
  const res = await getBoot();
  return {
    props: {
      profil: res.profil,
      produk: res.produk,
      solusi: res.solusi,
    }
  }
}