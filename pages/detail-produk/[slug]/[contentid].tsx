import { detailProduk } from "@/templates/Api/PortalController";
import Footer from "@/templates/Components/Footer";
import Navbar from "@/templates/Components/Navbar";
import DetailProdukTemp from "@/templates/ListProduk/DetailProdukTemp";
import { NextPage } from "next";
import Head from "next/head";
import Script from "next/script";

const Index: NextPage = (props: any) => {
  const { profil, produk, solusi, detailProduk } = props;
  const profileSite = { profil, produk, solusi };
  return (
    <>
      <Head>
        <title>Onebox - Solusi</title>
        <meta name="description" content="Portal Kecamatan Kota Depok" />
      </Head>

      <Script src="/js/plugins.js" />
      <Script src="/js/theme.js" />

      <Navbar {...profileSite} />

      <DetailProdukTemp {...props} />

      <Footer {...profileSite} />
    </>
  )
}

export default Index;

export async function getServerSideProps(context: any) {
  const { slug, contentid } = context.query;
  const res = await detailProduk(slug, contentid);
  return {
    props: {
      profil: res.profil,
      produk: res.produk,
      solusi: res.solusi,
      detailProduk: res.detailProduk,
      galeri: res.galeri,
      parents: res.parents,
    }
  }
}