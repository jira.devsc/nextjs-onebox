import { NextPage } from "next";
import Head from "next/head";
import HomePage from "@/templates/HomePage/HomePage";
import { fetchHome } from "@/templates/Api/PortalController";

const Index: NextPage = (props: any) => {
  return (
    <>
      <Head>
        <title>Index Page</title>
        <meta name="description" content="Portal Kecamatan Kota Depok" />
      </Head>
      <HomePage {...props} />
    </>
  );
};

export default Index;

export async function getServerSideProps() {
  const res = await fetchHome();
  return {
      props: {
        profil: res.profil,
        produk: res.produk,
        solusi: res.solusi,
        slider: res.slider,
        artikel: res.artikel,
        artikel1: res.artikel1,
        menu: res.menu,
        modul_exp: res.modul_exp,
      }
  }
}