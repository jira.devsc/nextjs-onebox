// components/TranslateWidget.js
import React, { useEffect } from 'react';

const TranslateWidget = () => {
  useEffect(() => {
    window.gtranslateSettings = {
      "default_language": "id",
      "native_language_names": true,
      "detect_browser_language": true,
      "languages": ["id", "en"],
      "wrapper_selector": ".gtranslate_wrapper"
    };

    const script = document.createElement('script');
    script.src = 'https://cdn.gtranslate.net/widgets/latest/float.js';
    script.async = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return <div className="gtranslate_wrapper"></div>;
};

export default TranslateWidget;
