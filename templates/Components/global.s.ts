// global.d.ts
interface Window {
  gtranslateSettings?: {
    default_language: string;
    native_language_names: boolean;
    detect_browser_language: boolean;
    languages: string[];
    wrapper_selector: string;
    // tambahkan properti lain sesuai kebutuhan
  };
}
