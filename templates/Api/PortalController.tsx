export async function getBoot() {
    var res, textRes;

    res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/profilsite?siteId=95");
    textRes = await res.text();
    const profil = textRes ? JSON.parse(textRes) : null;

    res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K010&limit=&offset=&category=222&slug=&key=&parent=");
    textRes = await res.text();
    const navbarProduk = textRes ? JSON.parse(textRes) : null;

    res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K005&limit=&offset=&category=&slug=&key=&contentId=");
    textRes = await res.text();
    const navbarSolusi = textRes ? JSON.parse(textRes) : null;

    const result = {
        profil,
        produk: navbarProduk,
        solusi: navbarSolusi
    }
    return result;
}
export async function fetchHome() {
    var res, textRes;

    res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/getSlider?siteId=105&status=ST01&kanalType=K010&groupId=&parent=&limit=3");
    textRes = await res.text();
    const slider = textRes ? JSON.parse(textRes) : null;

    res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K003&limit=4&offset=&category=&slug=&key=&contentId=");
    textRes = await res.text();
    const artikel = textRes ? JSON.parse(textRes) : null;

    res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K003&limit=1&offset=&category=&slug=&key=&contentId=");
    textRes = await res.text();
    const artikel1 = textRes ? JSON.parse(textRes) : null;

    // res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/getExLink?siteId=105&code=&groupId=&typeId=EP&limit=&offset=&slug=&parent=");
    // textRes = await res.text();
    const menu = textRes ? JSON.parse(textRes) : null;

    res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K010&limit=&offset=&category=141&slug=&key=&content_id=243");
    textRes = await res.text();
    const modul_exp = textRes ? JSON.parse(textRes) : null;

    const boot = await getBoot();
    const returnData = { ...boot, slider, artikel, artikel1, menu, modul_exp };
    return returnData;
}

// -- ARTIKEL
export async function listArtikel() {
    var res, textRes;

    res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K003&limit=&offset=&category=&slug=&key=");
    textRes = await res.text();
    const artikel = textRes ? JSON.parse(textRes) : null;

    const boot = await getBoot();
    const returnData = { ...boot, artikel };
    return returnData;
}

export async function detailArtikel(slug: string) {
    var res, textRes;

    res = await fetch(`https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K003&limit=&offset=&category=&slug=${slug}&key=`);
    textRes = await res.text();
    const detailArtikel = textRes ? JSON.parse(textRes) : null;

    const site = "https://dummy.smartcity.co.id/onebox-laravel/public/";

    res = await fetch(`https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K003&limit=&offset=&category=&slug=&key=`);
    textRes = await res.text();
    const artikelSlide = textRes ? JSON.parse(textRes) : null;

    const boot = await getBoot();
    const returnData = { ...boot, detailArtikel, site, artikelSlide };
    return returnData;
}

// KASUS
export async function listKasus() {
    var res, textRes;

    // res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/getExLink?siteId=105&code=&groupId=&typeId=EP&limit=&offset=&slug=&parent=");
    // textRes = await res.text();
    // const menu = textRes ? JSON.parse(textRes) : null;
    const menu = null;

    const boot = await getBoot();
    const returnData = { ...boot, menu };
    return returnData;
}

export async function detailSolusi(slug: string) {
    try {
        var res, textRes;

        res = await fetch(`https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K005&limit=&offset=&category=&slug=${slug}&key=`);
        textRes = await res.text();
        const detailSolusi = textRes ? JSON.parse(textRes) : null;

        const code = detailSolusi[0].Category.replaceAll(/ /g, '%20');
        res = await fetch(`https://cms.ciptadrasoft.com/ViewPortal/getExLink?siteId=105&code=${code}&groupId=&typeId=LM&limit=&offset=&slug=&parent=`);
        textRes = await res.text();
        const portotele = textRes ? JSON.parse(textRes) : null;


        const boot = await getBoot();
        const returnData = { ...boot, detailSolusi, portotele };
        return returnData;
    } catch (error) {
        console.error('Error in detailSolusi:', error);
        throw new Error('Error Fetching Data');
    }
}

// PRODUK
export async function listProduk() {
    var res, textRes;

    res = await fetch("https://cms.ciptadrasoft.com/ViewPortal/getExLink?siteId=105&code=&groupId=&typeId=EP&limit=&offset=&slug=&parent=");
    textRes = await res.text();
    // const menu = textRes ? JSON.parse(textRes) : null;
    const menu = null;

    const boot = await getBoot();
    const returnData = { ...boot, menu };
    return returnData;
}

export async function detailProduk(slug: string, contentid: string) {
    try {
        var res, textRes;

        res = await fetch(`https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K010&limit=&offset=&category=&slug=${slug}&contentId=${contentid}&key=`);
        textRes = await res.text();
        const detailProduk = textRes ? JSON.parse(textRes) : null;

        const code = detailProduk[0].sumber_informasi.replaceAll(/ /g, '%20');
        res = await fetch(`https://cms.ciptadrasoft.com/ViewPortal/getExLink?siteId=105&code=${code}&groupId=&typeId=LM&limit=&offset=&slug=&parent=`);
        textRes = await res.text();
        const galeri = textRes ? JSON.parse(textRes) : null;
        
        res = await fetch(`https://cms.ciptadrasoft.com/ViewPortal/get_content?siteId=105&status=ST01&kanalType=K010&limit=&offset=&category=&slug=&parent=${contentid}&key=`);
        textRes = await res.text();
        const parents = textRes ? JSON.parse(textRes) : null;

        const boot = await getBoot();
        const returnData = { ...boot, detailProduk, galeri, parents };
        return returnData;
    } catch (error) {
        console.error('Error in detailProduk:', error);
        throw new Error('Error Fetching Data');
    }
}