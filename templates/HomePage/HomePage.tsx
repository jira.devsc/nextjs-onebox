import Script from "next/script";
import Image from "next/image";
import Footer from "../Components/Footer";
import Navbar from "../Components/Navbar";
import SliderTop from "./SliderTop";
import { useEffect } from "react";
import Link from "next/link";

const HomePage = (props: any) => {
const { profil, produk, solusi, slider, artikel, artikel1, menu, modul_exp } = props;
const profileSite = { profil, produk, solusi };

useEffect(() => {
// @ts-ignore
const solusi1 = new Splide('#solusi1', {
type: 'loop',
perPage: 1,
autoplay: true,
}).mount();
// @ts-ignore
const solusi2 = new Splide('#solusi2', {
type: 'loop',
perPage: 1,
autoplay: true,
}).mount();
// @ts-ignore
const solusi3 = new Splide('#solusi3', {
type: 'loop',
perPage: 1,
autoplay: true,
}).mount();
// @ts-ignore
const portofolio = new Splide('#portofolio', {
type: 'loop',
perPage: 1,
autoplay: true,
}).mount();

return () => {
solusi1.destroy();
solusi2.destroy();
solusi3.destroy();
portofolio.destroy();
}
}, []);

return (
<div>
  <Navbar {...profileSite} />
  <Script src="/js/plugins.js" />
  <Script src="/js/theme.js" />
  <section className="opening wrapper bg-light position-relative min-vh-60 d-lg-flex align-items-center">
    <SliderTop slider={slider} />
  </section>


  <section className="deskripsi-wrapper">
    <div className="container py-9 py-md-8 pb-md-8">

      <div className="col-lg-12">
        <p className="font-desc" style={{ color: "white" }}>Kemudahan meningkatkan layanan pelanggan dari semua
          channel, mempercepat penyelesaian pengaduan, meningkatkan produktitas kerja, penjualan dengan operasional
          lebih mudah, cepat dan hemat, terintegrasi dengan task management, knowledge sistem dan data pelanggan</p>
        <div className="row">
          <section className="product-featured">
            <div className="container">
              <ul className="nav nav-tabs nav-tabs-basic justify-content-center">
                <li className="nav-item">
                  <Link className="nav-link active" data-bs-toggle="tab" href="#tab3-1" title="Customer Experience"><i
                    className="bx bx-desktop"></i><br />Customer Experience</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" data-bs-toggle="tab" href="#tab3-2" title="Marketing & Sales"> <i
                    className="bx bx-cart"></i><br />Marketing & Sales</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" data-bs-toggle="tab" href="#tab3-3" title="Customer"
                    style={{ marginRight: 0 }}> <i className="bx bx-conversation"></i><br />Customer Feedback</Link>
                </li>
              </ul>
            </div>
            {/*
            <!-- /.nav-tabs --> */}
            <div className="tab-content">
              <div className="tab-pane fade show active" id="tab3-1">
                <div className="solusi-wrapper1">
                  <div className="container">
                    <div className="solusi-bg">
                      <div className="row d-flex justify-content-around">
                        <div className="col-lg-6 d-flex align-items-center">
                          <div id="solusi1" className="splide" role="group" aria-label="Splide Basic HTML Example">
                            <div className="splide__track">
                              <ul className="splide__list">
                                <li className="splide__slide">
                                  <img src="/img/illustrations/gambar custex/custex1.svg"
                                    srcSet="/img/illustrations/gambar custex/ilustrasi 1.webp" alt="Customer Experience"
                                    width="100%" loading="lazy" />
                                </li>
                                <li className="splide__slide">
                                  <img src="/img/illustrations/gambar custex/ilustrasi 2.webp"
                                    srcSet="/img/illustrations/gambar custex/ilustrasi 2.webp" alt="Customer Experience"
                                    width="100%" loading="lazy" />
                                </li>
                                <li className="splide__slide">
                                  <img src="/img/illustrations/gambar custex/noSideMonitoring.webp"
                                    srcSet="/img/illustrations/gambar custex/ilustrasi 2.webp" alt="Customer Experience"
                                    width="100%" loading="lazy" />
                                </li>

                              </ul>
                            </div>
                          </div>
                        </div>

                        <div className="col-lg-7 col-xl-5 offset-lg-1 d-flex align-items-center">
                          <div className="card-manfaat mb-4">
                            <div className="desc-pilih">
                              <div className="tt-el-caption text-left">
                                <h3>Customer <strong>Experience</strong></h3>
                              </div>
                              <p className="mb-2">Meningkatkan layanan contact center lebih mudah dan cepat, melayani
                                informasi dan pengaduan dari berbagai channel dalam satu screen dan didukung sumber daya
                                informasi yang dibutuhkan pelanggan.
                              </p>

                              <ul className="poin-modul cc-2">
                                <li><i className="bx bx-headphone"></i>
                                  <Link href={`/detail-produk/${produk[11].slug_title}/${produk[11].content_id}`}
                                    title="Layanan Pelanggan">Layanan Pelanggan</Link>
                                </li>
                                <li><i className="bx bx-task"></i>
                                  <Link href={`/detail-produk/${produk[9].slug_title}/${produk[9].content_id}`}>Manajemen Penugasan</Link>
                                </li>
                                <li><i className="bx bx-bot"></i>
                                  <Link href={`/detail-produk/${produk[2].slug_title}/${produk[2].content_id}`}
                                    title="Chatbot">Chatbot</Link>
                                </li>
                                <li><i className="bx bx-link"></i>
                                  <Link href={`/detail-produk/${produk[10].slug_title}/${produk[10].content_id}`}
                                    title="Customer Relationship">Customer Relationship</Link>
                                </li>
                                <li><i className="bx bx-globe"></i>
                                  <Link href={`/detail-produk/${produk[5].slug_title}/${produk[5].content_id}`}
                                    title="Customer Portal/ Mobile">Customer Portal/ Mobile</Link>
                                </li>
                                <li><i className="bx bxl-instagram-alt"></i>
                                  <Link href={`/detail-produk/${produk[7].slug_title}/${produk[7].content_id}`}
                                    title="API Social Media">Sosial Media API</Link>
                                </li>
                              </ul>
                            </div>
                          </div>

                        </div>
                        {/*
                        <!--/column --> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/*
              <!--/.tab-pane --> */}
              <div className="tab-pane fade" id="tab3-2">
                <div className="container py-md-5">
                  <div className="solusi-wrapper2">
                    <div className="row d-flex justify-content-around">

                      <div className="col-lg-6 d-flex align-items-center">
                        <div id="solusi2" className="splide" role="group" aria-label="Splide Basic HTML Example">
                          <div className="splide__track">
                            <ul className="splide__list">
                              <li className="splide__slide">
                                <img src="/img/illustrations/gambar marsel/prospect.webp"
                                  srcSet="/img/illustrations/gambar marsel/prospect.webp" alt="Marketing dan Sales"
                                  width="100%" loading="lazy" />
                              </li>
                              <li className="splide__slide">
                                <img src="/img/illustrations/gambar marsel/tg3-1.webp"
                                  srcSet="/img/illustrations/gambar marsel/tg3-1.webp" alt="Marketing dan Sales"
                                  width="100%" loading="lazy" />
                              </li>
                              <li className="splide__slide">
                                <img src="/img/illustrations/gambar marsel/tg3-2.webp"
                                  srcSet="/img/illustrations/gambar marsel/tg3-2.webp" alt="Marketing dan Sales"
                                  width="100%" loading="lazy" />
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-7 col-xl-5 d-flex align-items-center">
                        <div className="card-manfaat mb-4">
                          <div className="desc-pilih">
                            <div className="tt-el-caption text-left">
                              <h3>Marketing & <strong>Sales</strong></h3>
                            </div>
                            <p className="mb-2">Berkomunikasi dengan prospek melalui banyak channel (Telephony,
                              Whatsapp, Facebook, Twitter, Instagram dan channel lainnya), sales dapat membuat reminder
                              janji, update status prospek, memasukan target dan nilai penjualan, serta semua kinerja
                              agent penjualan lebih terpantau.</p>

                            <ul className="poin-modul cc-2">
                              <li><i className="bx bx-shape-square"></i>
                                <Link href={`/detail-produk/${produk[4].slug_title}/${produk[4].content_id}`}
                                  title="Prospek Manjemen">Manajemen Prospek</Link>
                              </li>
                              <li><i className="bx bx-broadcast"></i>
                                <Link href={`/detail-produk/${produk[3].slug_title}/${produk[3].content_id}`}
                                  title="Broadcast">Broadcast</Link>
                              </li>
                              <li><i className="bx bx-menu-alt-left"></i>
                                <Link href={`/detail-produk/${produk[0].slug_title}/${produk[0].content_id}`}
                                  title="Sistem Antrian">Sistem Antrian</Link>
                              </li>
                            </ul>
                          </div>

                        </div>
                      </div>


                    </div>
                  </div>
                </div>
              </div>
              {/*
              <!--/.tab-pane --> */}
              <div className="tab-pane fade" id="tab3-3">
                <div className="solusi-wrapper1">
                  <div className="container py-md-5">
                    <div className="row d-flex justify-content-around">
                      <div className="col-lg-6 d-flex align-items-center">
                        <div id="solusi3" className="splide" role="group" aria-label="Splide Basic HTML Example">
                          <div className="splide__track">
                            <ul className="splide__list">
                              <li className="splide__slide">
                                <img src="/img/illustrations/gambar cusfed/feedback1.webp"
                                  srcSet="/img/illustrations/gambar cusfed/feedback1.webp" alt="Marketing dan Sales"
                                  width="100%" loading="lazy" />
                              </li>
                              <li className="splide__slide">
                                <img src="/img/illustrations/gambar cusfed/feedback2.webp"
                                  srcSet="/img/illustrations/gambar cusfed/feedback2.webp" alt="Marketing dan Sales"
                                  width="100%" loading="lazy" />
                              </li>
                              <li className="splide__slide">
                                <img src="/img/illustrations/gambar cusfed/feedback3.webp"
                                  srcSet="/img/illustrations/gambar cusfed/feedback3.webp" alt="Marketing dan Sales"
                                  width="100%" loading="lazy" />
                              </li>
                            </ul>
                          </div>
                        </div>

                      </div>
                      {/*
                      <!--/column --> */}
                      <div className="col-lg-6 col-xl-5 offset-lg-1 d-flex align-items-center">
                        <div className="card-manfaat mb-4">
                          <div className="desc-pilih">
                            <div className="tt-el-caption text-left">
                              <h3>Customer <strong>Feedback</strong></h3>
                            </div>
                            <p className="mb-2">Mengukur tingkat kepuasan layanan dan melakukan survey layanan yang
                              dilakukan melalui
                              broadcast melalui sosmed, email/wa, terkait apa yang perlu dilakukan perbaikan (melalui
                              website/aplikasi mobile, play store maupun google review).</p>

                            <ul className="poin-modul cc-2">
                              <li><i className="bx bx-conversation"></i>
                                <Link href={`/detail-produk/${produk[8].slug_title}/${produk[8].content_id}`}
                                  title="Customer Feedback">Customer Feedback</Link>
                              </li>
                              <li><i className="bx bx-desktop"></i>
                                <Link href={`/detail-produk/${produk[1].slug_title}/${produk[1].content_id}`}
                                  title="Media Monitoring">Media Monitoring</Link>
                              </li>
                            </ul>
                          </div>

                        </div>

                      </div>
                      {/*
                      <!--/column --> */}
                    </div>
                  </div>
                </div>
              </div>

              {/*
              <!--/.tab-pane --> */}
            </div>
          </section>
        </div>
      </div>
    </div>
  </section>

  <section className="section-content">
    <div className="pt-12" style={{ backgroundColor: "#f8f8f8" }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="tt-el-caption text-center mb-3">
              <h3>Melayani lebih baik, bekerja lebih mudah, produktifitas meningkat , penjualan bertambah bersama
                <strong>Onebox</strong>
              </h3>
            </div>
          </div>
        </div>
        <div className="img-produk">
          <img src="/img/illustrations/screen-img.png" alt="onebox" width="100%" loading="lazy" />
        </div>
      </div>
    </div>
  </section>

  <section className="benefit-wrapper mt-10 mb-12" id="demos">
    <div className="container">
      <div className="row mb-8">
        <div className="col-md-9 col-lg-7 col-xl-8 col-xxl-9 mx-auto text-center">
          <div className="tt-el-caption">
            <h2 className="display-4 mb-3 text-center">Manfaat</h2>
            <p className="display-7">Solusi Omnichannel untuk membangun <span>loyalitas pelanggan </span>dalam satu
              layar</p>
          </div>
        </div>
        {/*
        <!-- /column --> */}
      </div>
      {/*
      <!-- /.row --> */}
      <div className="benefit-sec">
        <div className="row service-group">
          <div className="col-xs-12 col-sm-3 col-md-3">
            <div className="tt-el-service shadow">
              <div className="icon-layanan">
                <img src="/img/icons/service.webp" alt="layanan" data-width="37px" loading="lazy" />
              </div>
              <p>
                Melayani <strong>lebih baik</strong>, bekerja <strong>lebih cepat </strong>dan mudah
              </p>
            </div>
          </div>
          <div className="col-xs-12 col-sm-3 col-md-3">
            <div className="tt-el-service shadow">
              <div className="icon-layanan">
                <img src="/img/icons/feedback.webp" alt="feedback" data-width="40px" loading="lazy" />
              </div>
              <p>
                Feedback pelanggan, Peta masalah, dan layanan <strong>lebih terukur</strong>
              </p>
            </div>
          </div>

          <div className="col-xs-12 col-sm-3 col-md-3">
            <div className="tt-el-service shadow">
              <div className="icon-layanan">
                <img src="/img/icons/layar.webp" alt="kanal" data-width="50px" loading="lazy" />
              </div>
              <p>
                Melayani semua kanal dalam <strong>satu layar</strong>
              </p>
            </div>
          </div>
          <div className="col-xs-12 col-sm-3 col-md-3">
            <div className="tt-el-service shadow">
              <div className="icon-layanan">
                <img src="/img/icons/kinerja.webp" alt="kinerja" data-width="50px" loading="lazy" />
              </div>
              <p>
                Kinerja agen, unit layanan lebih terukur untuk <strong>meningkatkan produktivitas</strong>
              </p>
            </div>
          </div>
          <div className="col-xs-12 col-sm-3 col-md-3">
            <div className="tt-el-service shadow">
              <div className="icon-layanan">
                <img src="/img/icons/diluar.webp" alt="operasional" data-width="50px" loading="lazy" />
              </div>
              <p>
                Operasional layanan <strong>lebih hemat </strong>dan dapat <strong>bekerja diluar kantor</strong>
              </p>
            </div>
          </div>
          <div className="col-xs-12 col-sm-3 col-md-3">
            <div className="tt-el-service shadow">
              <div className="icon-layanan">
                <img src="/img/icons/profil=0p-pp.webp" alt="profile" data-width="50px" loading="lazy" />
              </div>
              <p>
                Lebih mengenali <strong>profile </strong>dan <strong>riwayat pelanggan</strong>
              </p>
            </div>
          </div>
          <div className="col-xs-12 col-sm-3 col-md-3">
            <div className="tt-el-service shadow">
              <div className="icon-layanan">
                <img src="/img/icons/integrasi.webp" alt="integrasi" data-width="50px" loading="lazy" />
              </div>
              <p>
                <strong>Terintegrasi</strong> dengan knowledge, profile pelanggan, dan task manajemen
              </p>
            </div>
          </div>
          <div className="col-xs-12 col-sm-3 col-md-3">
            <div className="tt-el-service shadow">
              <div className="icon-layanan">
                <img src="/img/icons/branding.webp" alt="branding" data-width="50px" loading="lazy" />
              </div>
              <p>
                Membantu <strong>bisnis bertumbuh </strong>dalam layanan, branding dan penjualan
              </p>
            </div>
          </div>

        </div>
        {/*
        <!-- /.project --> */}
      </div>
    </div>
  </section>



  <section className="benefit-wrapper mt-10 pb-5" id="demos">
    <div className="container">
      <div className="row mb-8">
        <div className="col-md-9 col-lg-7 col-xl-8 col-xxl-9 mx-auto text-center">
          <div className="tt-el-caption">
            <h4 className="display-7">Artikel</h4>
          </div>
        </div>
        {/*
        <!-- /column --> */}
      </div>
      <div className="benefit-sec">
        <div className="row service-group">
          <div className="row">
            <div className="col-md-12">
              <div className="tt-el-blog-carousel text-light">
                <div className="owl-carousel">
                  <div className="owl-blog-item mb-3">
                    <div className="row">
                      {artikel && artikel.map((item: any, key: number) => (
                      <div className="col-lg-6" key={key}>
                        <div className="blog-item d-flex wow fadeInUp delay-0-4s mb-3">
                          <div className="blog-image-new">
                            <img src={`https://cms.ciptadrasoft.com/upload/${item.lampiran}`} alt="artikel"
                              loading="lazy" className="img-new" />
                          </div>
                          <div className="blog-content">
                            <span className="tanggal"><i className="bx bx-calendar"></i>{item.tgl_publish}</span>
                            <div className="content">
                              <h5 className="h5-artikel pt-0">
                                <Link href="{{ url('/detail-artikel').'/'.$item['slug_title'] }}" title="Artikel">
                                {(item.title || '').replace(/(<([^>]+)>)/gi, '').substring(0, 40)}</Link>
                              </h5>
                              <p className="p-artikel">{(item.content || '').replace(/(<([^>]+)>)/gi, '').substring(0,
                                  90)}</p>
                              <Link href={`/detail-artikel/${item.slug_title}`} title="Selengkapnya"
                                className="read-more">Selengkapnya <i className="uil uil-arrow-right"></i></Link>
                            </div>
                          </div>
                        </div>
                      </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="d-flex justify-content-center justify-content-lg-start mb-4" data-group="page-title-buttons">
            <Link className="feature-button" href="/list-artikel" title="Artikel Lainnya"
              style={{ padding: '10px 25px' }}>Lihat Lainnya</Link>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section className="benefit-wrapper" id="demos">
    <div className="pb-10 pt-12 container">
      <div className="row mb-8">
        <div className="col-md-9 col-lg-7 col-xl-8 col-xxl-9 mx-auto text-center">
          <div className="tt-el-caption mb-0">
            <h4 className="display-7">Frequently Asked Questions</h4>
          </div>
        </div>
        {/*
        <!-- /column --> */}
      </div>
      {/*
      <!-- /.row --> */}
      <div className="benefit-sec">
        <div className="row service-group">
          <div className="row">
            <div className="col-lg-6">
              <div className="accordion accordion-wrapper" id="accordionExample">
                <div className="card accordion-item">
                  <div className="card-header" id="headingOne">
                    <button className="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                      aria-expanded="true" aria-controls="collapseOne">Apa itu Onebox Omnichannel? </button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <p>Onebox Omnichannel adalah aplikasi yang digunakan untuk mengelola layanan
                        telepon, email, social media, webchat, walk in, web form dalam satu layar
                        aplikasi yang terintegrasi.</p>
                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}
                <div className="card accordion-item">
                  <div className="card-header" id="headingTwo">
                    <button className="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                      aria-expanded="false" aria-controls="collapseTwo">Apa manfaat dari Onebox
                      Omnichannel untuk bisnis/perusahaan?</button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <ul>
                        <li>Melayani lebih baik, bekerja lebih cepat dan mudah dari semua kanal
                          layanan dalam satu
                          layar didukung integrasi dengan data publik dan pusat knowledge.</li>
                        <li>Lebih mengenali dan memahami publik.</li>
                        <li>Feedback, Peta masalah, layanan lebih terukur untuk setiap jenis produk,
                          jenis layanan,
                          wilayah layanan sehingga menjadi sumber informasi yang baik untuk
                          evaluasi</li>
                        <li>Kinerja petugas, unit layanan lebih terukur, meningkatkan produktifitas
                        </li>
                        <li>Penyelesaian permintaan informasi dan aspirasi, pengaduan lebih cepat.
                        </li>
                        <li>Terintegrasi dengan task management yang difollowup unit lain</li>
                        <li>Operasional layanan lebih murah dengan kehadiran chatbot, portal+mobile
                          apps layanan
                          publik, mobile apps petugas yang dapat bekerja di lapangan</li>
                        <li>Membantu layanan bertumbuh lebih baik, makin dekat dengan public dan
                          branding
                          perusahaan</li>
                      </ul>
                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}
                <div className="card accordion-item">
                  <div className="card-header" id="headingThree">
                    <button className="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree"
                      aria-expanded="false" aria-controls="collapseThree">Bagaimana cara kerja Onebox
                      Omnichannel?</button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <p>Onebox Omnichannel bekerja dengan menghubungkan berbagai saluran komunikasi
                        dalam satu
                        platform terpadu, dan mengintegrasikannya dengan sistem CRM. Kemudian sistem
                        akan menyimpan
                        semua aktivitas secara otomatis. Sehingga semua aktivitas di berbagai
                        saluran komunikasi tersebut dapat tercatat dan dimonitor dengan mudah oleh
                        manajemen perusahaan.</p>
                      <p>Misalnya pelanggan ingin menanyakan informasi produk, pertanyaan tersebut
                        akan diubah sebagai
                        tiket yang bisa dilihat dari dashboard list ticket. Selanjutnya tim atau
                        agent bisa menindaklanjuti.
                        Aktivitas tersebut akan dibuat laporan dalam bentuk grafis maupun tabel.
                      </p>
                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}
                <div className="card accordion-item">
                  <div className="card-header" id="headingOne">
                    <button className="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapseFour"
                      aria-expanded="true" aria-controls="collapseFour">Apakah Onebox Omnichannel dapat disesuaikan
                      dengan
                      kebutuhan bisnis perusahaan?</button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingOne"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <p>Ya, Onebox Omnichannel sangat dapat disesuaikan untuk memenuhi kebutuhan
                        bisnis secara
                        spesifik. Anda dapat menyesuaikan sistem ini sesuai dengan kebutuhan Anda.
                        Solusi kami dirancang
                        agar fleksibel dan dapat berkembang, mampu mengakomodasi bisnis dari
                        berbagai ukuran dan
                        industri.
                      </p>
                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}
                <div className="card accordion-item">
                  <div className="card-header" id="headingOne">
                    <button className="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                      aria-expanded="true" aria-controls="collapseOne">Apakah Onebox Omnichannel cocok untuk berbagai
                      indutri dan skala bisnis?</button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <p>Ya, Onebox Omnichannel dirancang untuk melayani berbagai skala bisnis maupun
                        industri, mulai dari
                        startup kecil hingga perusahaan besar. Platform ini menawarkan skalabilitas
                        dan fleksibilitas untuk
                        memenuhi kebutuhan bisnis yang unik dan pertumbuhan yang berbeda-beda pada
                        setiap organisasi.
                        Selain itu, solusi kami dipakai oleh banyak industri dan perusahaan mulai
                        dari Bank, Ritel, Startup,
                        Telekomunikasi, organisasi non-profit, Logistik, Pendidikan, Layanan
                        Kesehatan, Badan Usaha Milik
                        Negara, pemerintahan hingga UMKM.
                      </p>
                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}
              </div>
              {/*
              <!--/.accordion --> */}
            </div>
            <div className="col-lg-6">
              <div className="accordion accordion-wrapper" id="accordionExample">
                <div className="card accordion-item">
                  <div className="card-header" id="headingFive">
                    <button className="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapseFive"
                      aria-expanded="true" aria-controls="collapseFive">Bagaimana cara memulai menggunakan Onebox
                      Omnichannel? </button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseFive" className="accordion-collapse collapse" aria-labelledby="headingFive"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <p>Hubungi kami dan daftar hari ini, tim kami akan menghubungi anda</p>
                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}
                <div className="card accordion-item">
                  <div className="card-header" id="headingSix">
                    <button className="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSix"
                      aria-expanded="false" aria-controls="collapseSix">Siapa saja yang dapat
                      menggunakan Onebox Omnichannel?
                    </button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseSix" className="accordion-collapse collapse" aria-labelledby="headingSix"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <p>Berikut beberapa pihak yang dapat menggunakan Onebox Omnichannel:</p>
                      <ul>
                        <li>Perusahaan (Enterprise)</li>
                        <li>Pemilik bisnis</li>
                        <li>UMKM (Usaha Mikro Kecil Menengah)</li>
                        <li>Pengusaha</li>
                        <li>Influencer</li>
                        <li>Organisasi atau dinas pemerintah</li>
                        <li>Lembaga pendidikan</li>
                        <li>dan lainnya</li>
                      </ul>
                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}
                <div className="card accordion-item">
                  <div className="card-header" id="headingSeven">
                    <button className="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSeven"
                      aria-expanded="false" aria-controls="collapseSeven">Apa manfaat Onebox
                      Omnichannel untuk pelanggan? </button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseSeven" className="accordion-collapse collapse" aria-labelledby="headingSeven"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <ul>
                        <li>Mendapatkan pelayanan yang cepat dan responsif.</li>
                        <li>Bisnis yang menggunakan CRM dapat memberikan pelayanan yang cepat dan
                          responsif
                          sehingga pelanggan akan merasa puas.</li>
                        <li>Pelanggan pun dapat mengetahui solusi dari kendala yang dihadapi dengan
                          cepat dan tidak perlu menunggu dalam waktu lama.</li>
                        <li>Solusi atau kebutuhan dapat terselesaikan dengan cepat CRM membuat
                          pelanggan bisa
                          mendapatkan penawaran barang atau jasa yang sesuai dengan kebutuhan dan
                          keinginan
                          berdasarkan riwayat aktivitas pelanggan.</li>
                        <li>Memudahkan dalam menghubungi bisnis dalam berbagai channel.</li>
                      </ul>
                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}

                <div className="card accordion-item">
                  <div className="card-header" id="headingEight">
                    <button className="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseEight"
                      aria-expanded="false" aria-controls="collapseEight">Bisakah mencoba Onebox
                      Omnichannel secara gratis sebelum melakukan komitmen?
                    </button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseEight" className="accordion-collapse collapse" aria-labelledby="headingEight"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <p>Hubungi kami dan daftar hari ini, tim kami akan menghubungi anda.</p>
                      <p>Jangan ragu untuk menghubungi tim dukungan kami jika Anda membutuhkan bantuan
                        selama
                        periode uji coba atau memiliki pertanyaan lebih lanjut.</p>

                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}

                <div className="card accordion-item">
                  <div className="card-header" id="headingNine">
                    <button className="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseNine"
                      aria-expanded="false" aria-controls="collapseNine">Apa manfaat Onebox
                      Omnichannel untuk pelanggan? </button>
                  </div>
                  {/*
                  <!--/.card-header --> */}
                  <div id="collapseNine" className="accordion-collapse collapse" aria-labelledby="headingNine"
                    data-bs-parent="#accordionExample">
                    <div className="card-body">
                      <ul>
                        <li>Mendapatkan pelayanan yang cepat dan responsif.</li>
                        <li>Bisnis yang menggunakan CRM dapat memberikan pelayanan yang cepat dan
                          responsif
                          sehingga pelanggan akan merasa puas.</li>
                        <li>Pelanggan pun dapat mengetahui solusi dari kendala yang dihadapi dengan
                          cepat dan tidak perlu menunggu dalam waktu lama.</li>
                        <li>Solusi atau kebutuhan dapat terselesaikan dengan cepat CRM membuat
                          pelanggan bisa
                          mendapatkan penawaran barang atau jasa yang sesuai dengan kebutuhan dan
                          keinginan
                          berdasarkan riwayat aktivitas pelanggan.</li>
                        <li>Memudahkan dalam menghubungi bisnis dalam berbagai channel.</li>
                      </ul>
                    </div>
                    {/*
                    <!--/.card-body --> */}
                  </div>
                  {/*
                  <!--/.accordion-collapse --> */}
                </div>
                {/*
                <!--/.accordion-item --> */}
              </div>
              {/*
              <!--/.accordion --> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section className="benefit-wrapper testimoni-wrapper" id="demos">
    <div className="pb-10 pt-12 container">
      <div className="row mb-8">
        <div className="col-md-9 col-lg-7 col-xl-8 col-xxl-9 mx-auto text-center">
          <div className="tt-el-caption mb-0">
            <h4 className="display-7">Testimoni</h4>
          </div>
        </div>
        {/*
        <!-- /column --> */}
      </div>
      {/*
      <!-- /.row --> */}
      <div className="testimoni-sec">
        <div className="row service-group">
          <div className="col-lg-4">
            <div className="card shadow">
              <div className="logo">
                <img alt="layanan" data-width="37px" loading="lazy" src="/img/logo/sim.png" />
              </div>
              <h1>Annisa Riyantina</h1>
              <p>Officer Layanan Customer Service</p>

              <blockquote className="icon icon-top">
                <p>Manfaat paling nyata dari omni-channel contact center onebox
                  adalah peningkatan efisiensi operasional. Dengan omni-channel,
                  perusahaan dapat mengelola semua interaksi pelanggan dalam
                  satu platform. Hal ini dapat menghemat waktu dan sumber daya
                  perusahaan, sehingga memungkinkan mereka untuk memberikan
                  layanan yang lebih baik kepada pelanggan dengan biaya yang
                  lebih rendah.
                </p>
              </blockquote>
            </div>
          </div>

          <div className="col-lg-4">
            <div className="card shadow">
              <div className="logo">
                <img alt="layanan" data-width="37px" loading="lazy" src="/img/logo/ciputra.png" />
              </div>
              <h1>Danuwijaya HN</h1>
              <p>Manager CCC</p>

              <blockquote className="icon icon-top">
                <p>Sebelumnya, kontrol terhadap channel layanan dilakukan secara
                  manual & banyak tab yang dibuka. saat ini cukup monitoring pada
                  1 dashboard saja menggunakan Onebox. Ada beberapa case
                  category untuk pengiriman dokumen yang dapat dilakukan
                  langsung oleh staf CCC. Dengan adanya fitur-fitur di onebox,
                  beberapa case category permintaan nasabah dapat dikirimkan
                  langsung oleh agent CCC sehingga SLA : 1 HK (dapat langsung
                  dikirim pada saat sedang online).
                </p>
              </blockquote>
            </div>
          </div>

          <div className="col-lg-4">
            <div className="card shadow">
              <div className="logo">
                <img alt="layanan" data-width="37px" loading="lazy" src="/img/logo/psn.jpeg" />
              </div>
              <h1>Ardhani Erstya Rynie</h1>
              <p>Asisten Manager</p>

              <blockquote className="icon icon-top">
                <p>Sebelumnya kami kesulitan dalam menarik data dan melakukan
                  monitoring karena beberapa aplikasi yang harus kami open. dan
                  setelah menggunakan Onebox, live chat dapat di-provide oleh
                  Onebox, kami bisa memonitoring trafik channel dengan sangat
                  mudah. semua channel yang kami butuhkan saat ini sudah masuk
                  ke Onebox, maka itu sangat memudahkan kami untuk monitoring
                  dan reporting load pekerjaan agent.
                </p>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section className="portfolio-wrapper bg-light mt-44" id="portfolio">
    <div className="container py-14 py-md-5 pb-md-5 text-center">
      <div className="portfolio">
        <div className="row mb-5">
          <div className="tt-el-caption text-center">
            <h3>Telah dipercaya oleh <strong>berbagai perusahaan </strong></h3>
          </div>
        </div>

        <div className="client-wrapper">
          <div className="row justify-content-around">
            <div className="col-lg-10">
              <div id="portofolio" className="splide" role="group" aria-label="Splide Basic HTML Example">
                <div className="splide__track mt-5">
                  <ul className="splide__list">
                    <li className="splide__slide">
                      <div className="container">
                        <div
                          className="row row-cols-4 row-cols-sm-2 row-cols-md-3 row-cols-xl-6 row-cols-xxl-6 justify-content-center">
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c1.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c2.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c3.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c4.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c5.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c6.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c7.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c8.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c9.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c10.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c11.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c12.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c13.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c14.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c15.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c16.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c17.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c18.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li className="splide__slide">
                      <div className="container">
                        <div
                          className="row row-cols-4 row-cols-sm-2 row-cols-md-3 row-cols-xl-5 row-cols-xxl-5 justify-content-center">

                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c19.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c20.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c21.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c22.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c23.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c24.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c25.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c26.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                          <div className="col">
                            <div className="img">
                              <img src="/img/client/c27.webp" alt="portofolio" loading="lazy" />
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-lg-2 gariss mt-5">
              <div className="swiper-container dots-closer mb-6" data-margin="30" data-dots="true">
                <div className="swiper">
                  <div className="swiper-wrapper">
                    <div className="swiper-slide">
                      <blockquote className="icon icon-top text-end">
                        <p>Melayani lebih baik, bekerja lebih cepat dan mudah</p>
                      </blockquote>
                    </div>
                    {/*
                    <!--/.swiper-slide --> */}
                    <div className="swiper-slide">
                      <blockquote className="icon icon-top text-end">
                        <p>Feedback pelanggan, Peta masalah, dan layanan lebih terukur</p>
                      </blockquote>
                    </div>
                    {/*
                    <!--/.swiper-slide --> */}
                    <div className="swiper-slide">
                      <blockquote className="icon icon-top text-end">
                        <p>Melayani semua channel dalam satu layar</p>
                      </blockquote>
                    </div>
                    {/*
                    <!--/.swiper-slide --> */}
                  </div>
                  {/*
                  <!--/.swiper-wrapper --> */}
                </div>
                {/*
                <!-- /.swiper --> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <Footer {...profileSite} />
</div>
);
};

export default HomePage;