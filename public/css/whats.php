
    <link rel="preload" href="/nhp/_next/static/chunks/pages/homepage/whats-new/whats-new-page-cd1ab2019af310f55250.js"
        as="script" />

   

    <style id="__jsx-308082724">
        .base-header-component.jsx-308082724 .desktop-wrapper.jsx-308082724 {
            display: block;
        }

        .base-header-component.jsx-308082724 .mobile-wrapper.jsx-308082724 {
            display: none;
        }

        @media (max-width: 1229px) {
            .base-header-component.jsx-308082724 .desktop-wrapper.jsx-308082724 {
                display: none;
            }

            .base-header-component.jsx-308082724 .mobile-wrapper.jsx-308082724 {
                display: block;
            }
        }
    </style>
    <style id="__jsx-1029950058">
        .core-title {
            font-size: 2.25rem;
            font-weight: 400;
            line-height: 48px;
            word-break: break-word;
        }

        .core-title.xss {
            font-size: 0.6875rem;
            line-height: 32px;
            font-weight: 400;
        }

        .core-title.xs_m {
            font-size: 0.875rem;
            line-height: 32px;
            font-weight: 400;
        }

        .core-title.xs_ml {
            font-size: 1rem;
            line-height: 32px;
            font-weight: 400;
        }

        .core-title.xs_l {
            font-size: 1.125rem;
            line-height: 32px;
            font-weight: 400;
        }

        .core-title.xs {
            font-size: 1.375rem;
            line-height: 32px;
            font-weight: 400;
        }

        .core-title.sm {
            font-size: 1.75rem;
            line-height: 40px;
            font-weight: 400;
        }

        .core-title.md {
            font-size: 2.25rem;
            line-height: 48px;
            font-weight: 400;
        }

        .core-title.lg {
            font-size: 2.75rem;
            line-height: 60px;
            font-weight: 700;
        }

        .core-title.lgr {
            font-size: 3.375rem;
            line-height: 68px;
            font-weight: 700;
        }

        .core-title.xl {
            font-size: 4rem;
            line-height: 72px;
            font-weight: 700;
        }

        .core-title.xxl {
            font-size: 4.5rem;
            line-height: 85px;
            font-weight: 700;
        }

        .title-and-icon-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .title-and-icon-wrapper .icon-wrapper {
            margin-right: 14px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .title-and-icon-wrapper .icon-wrapper .picture-icon-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            height: 32px;
        }

        .title-and-icon-wrapper .icon-wrapper .picture-icon-wrapper .title-icon {
            height: 100%;
        }

        .title-and-icon-wrapper .icon-wrapper.xss .picture-icon-wrapper {
            height: 8px;
        }

        .title-and-icon-wrapper .icon-wrapper.xs_m .picture-icon-wrapper {
            height: 12px;
        }

        .title-and-icon-wrapper .icon-wrapper.xs .picture-icon-wrapper {
            height: 18px;
        }

        .title-and-icon-wrapper .icon-wrapper.sm .picture-icon-wrapper {
            height: 24px;
        }

        .title-and-icon-wrapper .icon-wrapper.md .picture-icon-wrapper {
            height: 32px;
        }

        .title-and-icon-wrapper .icon-wrapper.lg .picture-icon-wrapper {
            height: 38px;
        }

        .title-and-icon-wrapper .icon-wrapper.lgr .picture-icon-wrapper {
            height: 44px;
        }

        .title-and-icon-wrapper .icon-wrapper.xl .picture-icon-wrapper {
            height: 56px;
        }

        .title-and-icon-wrapper .icon-wrapper.xxl .picture-icon-wrapper {
            height: 62px;
        }

        .title-and-icon-wrapper .icon-wrapper.lgr,
        .title-and-icon-wrapper .icon-wrapper.xl,
        .title-and-icon-wrapper .icon-wrapper.xxl {
            margin-right: 20px;
        }

        @media (max-width: 450px) {
            .core-title {
                font-size: 2.25rem;
                line-height: 48px;
                font-weight: 400;
            }

            .core-title.md,
            .core-title.lg {
                font-size: 1.75rem;
                line-height: 40px;
                font-weight: 700;
            }

            .core-title.xl,
            .core-title.lgr {
                font-size: 2.25rem;
                line-height: 48px;
                font-weight: 700;
            }

            .core-title.xxl {
                font-size: 2.75rem;
                line-height: 48px;
                font-weight: 700;
            }

            .title-and-icon-wrapper .icon-wrapper .picture-icon-wrapper {
                height: 32px;
            }

            .title-and-icon-wrapper .icon-wrapper.md .picture-icon-wrapper,
            .title-and-icon-wrapper .icon-wrapper.lg .picture-icon-wrapper,
            .title-and-icon-wrapper .icon-wrapper.xl .picture-icon-wrapper {
                height: 24px;
            }
        }
    </style>
    <style id="__jsx-3417310760">
        .paragraph-body {
            font-size: 1.125rem;
            line-height: 32px;
            margin: 0;
            word-break: break-word;
        }

        .paragraph-body.xs {
            font-size: 0.8125rem;
            line-height: 20px;
        }

        .paragraph-body.sm {
            font-size: 0.875rem;
            line-height: 22px;
        }

        .paragraph-body.ms {
            font-size: 1rem;
            line-height: 24px;
        }

        .paragraph-body.md {
            font-size: 1.125rem;
            line-height: 32px;
        }

        .paragraph-body.lg {
            font-size: 1.375rem;
            line-height: 40px;
        }
    </style>
    <style id="__jsx-1667800273">
        .full-paragraph {
            word-break: break-word;
            width: 100%;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .full-paragraph.align-items-left {
            -webkit-align-items: flex-start;
            -webkit-box-align: flex-start;
            -ms-flex-align: flex-start;
            align-items: flex-start;
        }

        .full-paragraph.align-items-center {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .full-paragraph.align-items-center .title-wrapper {
            width: 100%;
        }

        .full-paragraph.align-items-right {
            -webkit-align-items: flex-end;
            -webkit-box-align: flex-end;
            -ms-flex-align: flex-end;
            align-items: flex-end;
        }

        .full-paragraph.with-left-line {
            border-width: 2px;
            border-style: solid;
            padding-left: 32px;
        }

        .full-paragraph .full-paragraph-topic-image-wrapper {
            margin-bottom: 40px;
        }

        .full-paragraph .full-paragraph-topic {
            font-size: 0.875rem;
            margin-bottom: 16px;
        }

        .full-paragraph .title-wrapper {
            margin-top: 16px;
            margin-bottom: 16px;
        }

        .full-paragraph .icons-wrapper {
            margin-top: 40px;
            margin-bottom: 64px;
        }

        .full-paragraph .bullets-wrapper {
            margin-top: 32px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        @media (max-width: 768px) {
            .full-paragraph .bullets-wrapper.no-mobile-margin {
                margin-top: 0;
            }
        }

        .full-paragraph .bullets-wrapper.left {
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            -ms-flex-pack: start;
            justify-content: flex-start;
        }

        .full-paragraph .bullets-wrapper.left .bullets {
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            -ms-flex-pack: start;
            justify-content: flex-start;
        }

        .full-paragraph .bullets-wrapper.center {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .full-paragraph .bullets-wrapper.center .bullets {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .full-paragraph .bullets-wrapper.right {
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            -ms-flex-pack: end;
            justify-content: flex-end;
        }

        .full-paragraph .bullets-wrapper.right .bullets {
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            -ms-flex-pack: end;
            justify-content: flex-end;
        }

        .full-paragraph .full-paragraph-topic-image-wrapper.left .picture-component {
            -webkit-box-pack: left;
            -webkit-justify-content: left;
            -ms-flex-pack: left;
            justify-content: left;
        }

        .full-paragraph .full-paragraph-topic-image-wrapper.center .picture-component {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .full-paragraph .full-paragraph-topic-image-wrapper.right .picture-component {
            -webkit-box-pack: right;
            -webkit-justify-content: right;
            -ms-flex-pack: right;
            justify-content: right;
        }

        .full-paragraph .button-wrapper {
            margin-top: 32px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .full-paragraph .button-wrapper.left {
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            -ms-flex-pack: start;
            justify-content: flex-start;
        }

        .full-paragraph .button-wrapper.center {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .full-paragraph .button-wrapper.right {
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            -ms-flex-pack: end;
            justify-content: flex-end;
        }
    </style>
    <style id="__jsx-1055366927">
        .full-paragraph-component.jsx-1055366927 {
            max-width: 1440px;
            margin: auto;
        }

        .full-paragraph-component.jsx-1055366927 .full-paragraph-wrapper.jsx-1055366927 {
            max-width: 1100px;
            margin: auto;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            padding: 0px 32px;
        }
    </style>
    <style id="__jsx-958797829">
        .highlight-card.jsx-958797829 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            width: 90%;
        }

        .highlight-card.jsx-958797829 .highlight-image.jsx-958797829 {
            width: 100%;
            height: auto;
            overflow: hidden;
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
        }

        .highlight-card.jsx-958797829 .highlight-image.jsx-958797829 img.jsx-958797829 {
            width: 100%;
            height: auto;
            object-fit: cover;
        }

        .highlight-card.jsx-958797829 .card-content.jsx-958797829 {
            border: 1px solid;
            border-color: #DCDFEC;
            border-top: none;
            border-bottom-left-radius: 8px;
            border-bottom-right-radius: 8px;
            width: calc(100% - 1px);
            padding: 12px;
        }

        .highlight-card.jsx-958797829 .card-content.jsx-958797829 .release-type-wrapper.jsx-958797829 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            margin-top: 16px;
            margin-bottom: 10px;
        }

        .highlight-card.jsx-958797829 .card-content.jsx-958797829 .release-type.jsx-958797829 {
            font-size: 0.75rem;
            border-radius: 4px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            margin-right: 8px;
            background-color: #F0F3FF;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            color: #5034FF;
            padding: 6px 12px;
        }

        .highlight-card.jsx-958797829 .card-content.jsx-958797829 .highlight-title.jsx-958797829 {
            font-weight: 600;
            font-size: 1rem;
            padding: 4px 8px 0px;
            text-align: left;
            line-height: 26px;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
        }

        .highlight-card.jsx-958797829 .card-content.jsx-958797829 .highlight-description.jsx-958797829 {
            font-size: 0.75rem;
            padding: 8px;
            padding-bottom: 0;
            text-align: left;
            line-height: 22px;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 4;
            -webkit-box-orient: vertical;
        }

        .highlight-card.jsx-958797829 .card-content.jsx-958797829 .button-container.jsx-958797829 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: left;
            -webkit-justify-content: left;
            -ms-flex-pack: left;
            justify-content: left;
            padding: 8px 8px 16px;
        }

        .highlight-card.jsx-958797829 .card-content.jsx-958797829 .button-container.jsx-958797829 .secondary-button .secondary-button-text {
            font-size: 0.75rem;
        }

        .highlight-card.jsx-958797829 .card-content.jsx-958797829 .button-place-holder.jsx-958797829 {
            height: 48px;
        }

        .highlight-card.is-desktop.jsx-958797829 {
            width: 95%;
            border: 1px solid #dcdfec;
            border-color: #DCDFEC;
            border-radius: 8px;
        }

        .highlight-card.is-desktop.jsx-958797829:hover {
            box-shadow: 0 0 16px rgba(29, 140, 242, 0.16);
        }

        .highlight-card.is-desktop.jsx-958797829 .card-content.jsx-958797829 {
            border: none;
            border-color: none;
        }

        .highlight-card.is-desktop.jsx-958797829 .card-content.jsx-958797829 .highlight-title.jsx-958797829 {
            -webkit-line-clamp: 1;
        }

        .highlight-card.is-desktop.jsx-958797829 .card-content.jsx-958797829 .highlight-description.jsx-958797829 {
            -webkit-line-clamp: 2;
        }
    </style>
    <style id="__jsx-3631246281">
        .whats-new-first-fold-component {
            padding-top: 60px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            position: relative;
        }

        .whats-new-first-fold-component .left-space {
            min-width: calc((100vw - 1440px) / 2);
            -webkit-box-flex: 1;
            -webkit-flex-grow: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
        }

        .whats-new-first-fold-component .left-section {
            width: 40%;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .whats-new-first-fold-component .left-section .buttons-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            padding-left: 32px;
            gap: 20px;
        }

        .whats-new-first-fold-component .left-section .buttons-wrapper .circle-with-arrow {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            width: 35px;
            height: 35px;
            border-radius: 50%;
            background-color: #ffffff;
            position: relative;
            border: 1px solid;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            font-size: 1.75rem;
            padding-top: 3px;
            cursor: default;
        }

        .whats-new-first-fold-component .left-section .buttons-wrapper .circle-with-arrow:not(.first-slide):not(.last-slide):hover {
            color: #ffffff;
            background-color: #000000;
            cursor: pointer;
        }

        .whats-new-first-fold-component .left-section .full-paragraph-component {
            margin: 100px 0 20px 0;
        }

        .whats-new-first-fold-component .carousel-wrapper {
            width: 60%;
        }

        .whats-new-first-fold-component .card {
            opacity: 0;
            -webkit-transition: opacity 0.5s ease-in-out;
            transition: opacity 0.5s ease-in-out;
        }

        .whats-new-first-fold-component .slick-slide.slick-active .card {
            opacity: 1;
        }
    </style>
    <style id="__jsx-4176601438">
        .asset-inner {
            position: relative;
        }

        .asset-inner .main-image {
            width: 100%;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .asset-inner .main-image.round {
            border-radius: 50%;
        }

        .asset-inner img.with-window-mask,
        .asset-inner .with-window-mask>video {
            width: 100%;
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
        }
    </style>
    <style id="__jsx-2773234104">
        .slick-slider {
            position: relative;
            display: block;
            box-sizing: border-box;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -ms-touch-action: pan-y;
            touch-action: pan-y;
            -webkit-tap-highlight-color: transparent;
        }

        .slick-list {
            position: relative;
            overflow: hidden;
            display: block;
            margin: 0;
            padding: 0;
        }

        .slick-list:focus {
            outline: none;
        }

        .slick-list.dragging {
            cursor: pointer;
            cursor: hand;
        }

        .slick-slider .slick-track,
        .slick-slider .slick-list {
            -webkit-transform: translate3d(0, 0, 0);
            -moz-transform: translate3d(0, 0, 0);
            -ms-transform: translate3d(0, 0, 0);
            -o-transform: translate3d(0, 0, 0);
            -webkit-transform: translate3d(0, 0, 0);
            -ms-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

        .slick-track {
            position: relative;
            left: 0;
            top: 0;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .slick-track:before,
        .slick-track:after {
            content: "";
            display: table;
        }

        .slick-track:after {
            clear: both;
        }

        .slick-loading .slick-track {
            visibility: hidden;
        }

        .slick-slide {
            float: left;
            height: 100%;
            min-height: 1px;
            display: none;
        }

        [dir="rtl"] .slick-slide {
            float: right;
        }

        .slick-slide img {
            display: block;
        }

        .slick-slide.slick-loading img {
            display: none;
        }

        .slick-slide.dragging img {
            pointer-events: none;
        }

        .slick-initialized .slick-slide {
            display: block;
        }

        .slick-loading .slick-slide {
            visibility: hidden;
        }

        .slick-vertical .slick-slide {
            display: block;
            height: auto;
            border: 1px solid transparent;
        }

        .slick-arrow.slick-hidden {
            display: none;
        }

        .slick-arrow.slick-prev:focus,
        .slick-arrow.slick-next:focus {
            outline: solid 2px #015ecc;
            border-radius: 4px;
        }
    </style>
    <style id="__jsx-4236049877">
        @charset "UTF-8";

        @font-face {
            font-family: "slick";
            src: url("./fonts/slick.eot");
            src: url("./fonts/slick.eot?#iefix") format("embedded-opentype"), url("./fonts/slick.woff") format("woff"), url("./fonts/slick.ttf") format("truetype"), url("./fonts/slick.svg#slick") format("svg");
            font-weight: normal;
            font-style: normal;
        }

        .slick-loading .slick-list {
            background: #fff url("./ajax-loader.gif") center center no-repeat;
        }

        .slick-prev,
        .slick-next {
            position: absolute;
            display: block;
            height: 20px;
            width: 20px;
            line-height: 0px;
            font-size: 0rem;
            cursor: pointer;
            background: transparent;
            color: transparent;
            top: 50%;
            -webkit-transform: translate(0, -50%);
            -ms-transform: translate(0, -50%);
            -webkit-transform: translate(0, -50%);
            -ms-transform: translate(0, -50%);
            transform: translate(0, -50%);
            padding: 0;
            border: none;
            outline: none;
        }

        .slick-prev:hover,
        .slick-prev:focus,
        .slick-next:hover,
        .slick-next:focus {
            outline: none;
            background: transparent;
            color: transparent;
        }

        .slick-prev:hover:before,
        .slick-prev:focus:before,
        .slick-next:hover:before,
        .slick-next:focus:before {
            opacity: 1;
        }

        .slick-prev.slick-disabled:before,
        .slick-next.slick-disabled:before {
            opacity: 0.25;
        }

        .slick-prev:before,
        .slick-next:before {
            font-family: "slick";
            font-size: 1.125rem;
            line-height: 1;
            color: white;
            opacity: 0.75;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .slick-prev {
            left: -25px;
        }

        [dir="rtl"] .slick-prev {
            left: auto;
            right: -25px;
        }

        .slick-prev:before {
            content: "←";
        }

        [dir="rtl"] .slick-prev:before {
            content: "→";
        }

        .slick-next {
            right: -25px;
        }

        [dir="rtl"] .slick-next {
            left: -25px;
            right: auto;
        }

        .slick-next:before {
            content: "→";
        }

        [dir="rtl"] .slick-next:before {
            content: "←";
        }

        .slick-dotted.slick-slider {
            margin-bottom: 30px;
        }

        .slick-dots {
            position: absolute;
            bottom: -25px;
            list-style: none;
            display: block;
            text-align: center;
            padding: 0;
            margin: 0;
            width: 100%;
        }

        .slick-dots li {
            position: relative;
            display: inline-block;
            height: 20px;
            width: 20px;
            margin: 0 5px;
            padding: 0;
            cursor: pointer;
        }

        .slick-dots li button {
            border: 0;
            background: transparent;
            display: block;
            height: 20px;
            width: 20px;
            outline: none;
            line-height: 0px;
            font-size: 0rem;
            color: transparent;
            padding: 5px;
            cursor: pointer;
        }

        .slick-dots li button:hover,
        .slick-dots li button:focus {
            outline: none;
        }

        .slick-dots li button:hover:before,
        .slick-dots li button:focus:before {
            opacity: 1;
        }

        .slick-dots li button:before {
            position: absolute;
            top: 0;
            left: 0;
            content: "•";
            width: 20px;
            height: 20px;
            font-family: "slick";
            font-size: 0.338rem;
            line-height: 20px;
            text-align: center;
            color: black;
            opacity: 0.25;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .slick-dots li.slick-active button:before {
            color: black;
            opacity: 0.75;
        }
    </style>
    <style id="__jsx-2288892074">
        .carousel .slick-slider .slick-dots {
            z-index: 100;
        }

        .carousel .slick-slider .slick-dots li {
            margin: 0px 4px;
        }

        .carousel .slick-slider .slick-dots li button {
            display: inline-block;
            width: 11px;
            height: 11px;
            border-radius: 50%;
            border: 1px solid #676879;
            opacity: 1;
            background-color: transparent;
        }

        .carousel .slick-slider .slick-dots li button::before {
            display: none;
        }

        .carousel .slick-slider .slick-dots li.slick-active button {
            background-color: #6161FF;
            border-color: #6161FF;
        }

        .carousel .slick-slider .slick-dots li:not(.slick-active):hover button {
            background-color: #c5c7d0;
        }

        .carousel .slick-slider.focus .slick-list .slick-track {
            padding: 48px 0;
        }

        .carousel .slick-slider.focus .slick-list .slick-track .slick-slide .carousel-item {
            outline: none;
            -webkit-transition: opacity 0.5s ease, -webkit-transform 0.5s ease;
            -webkit-transition: opacity 0.5s ease, transform 0.5s ease;
            transition: opacity 0.5s ease, transform 0.5s ease;
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
            opacity: 0.3;
        }

        .carousel .slick-slider.focus .slick-list .slick-track .slick-slide.slick-active.slick-center .carousel-item {
            -webkit-transform: scale(1.175);
            -ms-transform: scale(1.175);
            transform: scale(1.175);
        }

        .carousel .slick-slider.focus .slick-list .slick-track .slick-slide.slick-active .carousel-item {
            opacity: 1;
        }
    </style>
    <style id="__jsx-2022567282">
        .use-cases-tabs-component {
            max-width: 1440px;
            margin: auto;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .use-cases-tabs-component .use-cases-tabs-navigation {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            gap: 8px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            max-width: 991px;
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }

        .use-cases-tabs-component .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            font-size: 0.875rem;
            font-weight: 500;
            padding: 8px 16px;
            border-radius: 20px;
            border: 1px solid #5034FF;
            white-space: nowrap;
            -webkit-transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out;
            transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out;
        }

        .use-cases-tabs-component .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            background-color: #F0F3FF;
            color: #5034FF;
            cursor: pointer;
        }

        .use-cases-tabs-component .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: #5034FF;
            color: #ffffff;
        }

        .use-cases-tabs-component .carousel-wrapper {
            width: calc(1440px - 48px);
            max-width: calc(100vw - 48px);
            -webkit-align-self: center;
            -ms-flex-item-align: center;
            align-self: center;
        }

        .use-cases-tabs-component .carousel-wrapper .slick-slide[aria-hidden="true"] .use-case-product-banner-button .button-component {
            display: none;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-item {
            text-align: center;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-item .use-case-tab-content {
            padding: 0 48px;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-item .use-case-tab-content .use-case-product-banner .use-case-product-banner-text .full-paragraph .title-wrapper {
            margin-bottom: 0;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-item .use-case-tab-content .use-case-product-banner .use-case-product-banner-text .full-paragraph .title-wrapper .core-title {
            font-size: 1.125rem;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-item .use-case-tab-content .use-case-product-banner .use-case-product-banner-button .regular-button {
            width: -webkit-max-content;
            width: -moz-max-content;
            width: max-content;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-arrow-wrapper {
            cursor: pointer;
            z-index: 9;
            text-align: center;
            width: 64px;
            height: 64px;
            outline: revert;
            top: 22vw;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-arrow-wrapper:before {
            display: none;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-arrow-wrapper:hover svg path {
            fill: #6161FF;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-arrow-wrapper svg {
            height: 100%;
            width: 16px;
        }

        .use-cases-tabs-component .carousel-wrapper .carousel-arrow-wrapper svg path {
            fill: #323338;
            -webkit-transition: fill 0.2s ease-in-out;
            transition: fill 0.2s ease-in-out;
        }

        .use-cases-tabs-component.projects-orange .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border: 1px solid #F86700;
        }

        .use-cases-tabs-component.projects-orange .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            color: #F86700;
            background-color: #FFC960;
        }

        .use-cases-tabs-component.projects-orange .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: #F86700;
            color: #ffffff;
        }

        .use-cases-tabs-component.projects-orange .carousel-wrapper .carousel-arrow-wrapper:hover svg path {
            fill: #FF9900;
        }

        .use-cases-tabs-component.white .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border: 1px solid #676879;
        }

        .use-cases-tabs-component.white .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            border-color: #ffffff;
            color: #ffffff;
            background-color: transparent;
        }

        .use-cases-tabs-component.white .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: rgba(255, 255, 255, 0.2);
            border-color: rgba(255, 255, 255, 0.2);
            color: #ffffff;
        }

        .use-cases-tabs-component.dev-lighter-tint-01 .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border: 1px solid #C6E9D5;
        }

        .use-cases-tabs-component.dev-lighter-tint-01 .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            border-color: #00CA72;
            color: #333333;
            background-color: transparent;
        }

        .use-cases-tabs-component.dev-lighter-tint-01 .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            color: #333333;
            background-color: #E2F0E7;
            border-color: #E2F0E7;
        }

        .use-cases-tabs-component.crm-green .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border: 1px solid #007474;
        }

        .use-cases-tabs-component.crm-green .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            color: #007474;
            background-color: #D3F4F4;
        }

        .use-cases-tabs-component.crm-green .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: #007474;
            color: #ffffff;
        }

        .use-cases-tabs-component.crm-green .carousel-wrapper .carousel-arrow-wrapper:hover svg path {
            fill: #00D2D2;
        }

        .use-cases-tabs-component.dev-green .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border: 1px solid #037F4C;
        }

        .use-cases-tabs-component.dev-green .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            color: #037F4C;
            background-color: #CAF4E2;
        }

        .use-cases-tabs-component.dev-green .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: #037F4C;
            color: #ffffff;
        }

        .use-cases-tabs-component.dev-green .carousel-wrapper .carousel-arrow-wrapper:hover svg path {
            fill: #00C875;
        }

        .use-cases-tabs-component.marketing-red .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border: 1px solid #CA0C6B;
        }

        .use-cases-tabs-component.marketing-red .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            color: #CA0C6B;
            background-color: #F8C3DD;
        }

        .use-cases-tabs-component.marketing-red .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: #CA0C6B;
            color: #ffffff;
        }

        .use-cases-tabs-component.marketing-red .carousel-wrapper .carousel-arrow-wrapper:hover svg path {
            fill: #F04095;
        }

        .use-cases-tabs-component.workos-iris .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border: 1px solid #5034FF;
        }

        .use-cases-tabs-component.workos-iris .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            color: #5034FF;
            background-color: #B4B4FF;
        }

        .use-cases-tabs-component.workos-iris .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: #5034FF;
            color: #ffffff;
        }

        .use-cases-tabs-component.workos-iris .carousel-wrapper .carousel-arrow-wrapper:hover svg path {
            fill: #6161FF;
        }

        .use-cases-tabs-component.crm-dark-tint-01 .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border: 1px solid #00889B;
        }

        .use-cases-tabs-component.crm-dark-tint-01 .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            color: #00889B;
            background-color: #E1EFF2;
        }

        .use-cases-tabs-component.crm-dark-tint-01 .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: #00889B;
            color: #ffffff;
        }

        .use-cases-tabs-component.crm-dark-tint-01 .carousel-wrapper .carousel-arrow-wrapper:hover svg path {
            fill: #00889B;
        }

        .use-cases-tabs-component.crm-darker-tint-02 .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border: 1px solid #006278;
        }

        .use-cases-tabs-component.crm-darker-tint-02 .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            color: #006278;
            background-color: #E1EFF2;
        }

        .use-cases-tabs-component.crm-darker-tint-02 .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: #006278;
            color: #ffffff;
        }

        .use-cases-tabs-component.crm-darker-tint-02 .carousel-wrapper .carousel-arrow-wrapper:hover svg path {
            fill: #006278;
        }

        @media (min-width: 1440px) {
            .use-cases-tabs-component .carousel-wrapper .carousel-arrow-wrapper {
                top: 305px;
            }
        }

        @media (max-width: 1440px) {
            .images-grid .grid-image-container {
                width: calc(28vw - 32px) !important;
            }
        }
    </style>
    <style id="__jsx-2062829799">
        .whats-new-apps-section {
            max-width: 1440px;
            margin: auto;
            padding-left: 2vw;
        }

        .whats-new-apps-section .subtle-line {
            border: 0;
            border-top: 1px solid;
            border-color: #F0F3FF;
            margin: 40px 0;
        }

        .whats-new-apps-section .title-wrapper {
            text-align: left;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .whats-new-apps-section .title-wrapper .core-title {
            font-size: 1.5rem;
            font-weight: 300;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component {
            -webkit-align-items: baseline;
            -webkit-box-align: baseline;
            -ms-flex-align: baseline;
            align-items: baseline;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper {
            border-color: Black;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper:hover {
            border-color: Black;
            color: black;
            background-color: transparent;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected {
            background-color: Black;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-cases-tabs-navigation .use-cases-tab-nav-button-wrapper.selected:hover {
            border-color: Black;
            color: white;
            background-color: Black;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .carousel-wrapper {
            width: 1440px;
            padding-left: 1vw;
        }

        @media (max-width: 1440px) {
            .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .carousel-wrapper {
                padding-left: 0;
            }
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-case-tab-content {
            padding-left: 0;
            height: 120px;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-case-tab-content .app-section {
            max-height: 200px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-case-tab-content .app-section a {
            all: unset;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-case-tab-content .app-section .app {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            gap: 20px;
            margin-bottom: 30px;
            cursor: pointer;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-case-tab-content .app-section .app .app-image-wrapper {
            width: 60px;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-case-tab-content .app-section .app .app-image-wrapper .asset-inner .picture-component img {
            border-radius: 12px;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-case-tab-content .app-section .app .text-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            text-align: left;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-case-tab-content .app-section .app .text-wrapper .app-title {
            font-size: 0.875rem;
            font-weight: 700;
            line-height: 21px;
        }

        .whats-new-apps-section .tabs-wrapper .use-cases-tabs-component .use-case-tab-content .app-section .app .text-wrapper .app-description {
            font-size: 0.875rem;
            font-weight: 300;
            line-height: 21px;
            width: 260px;
        }
    </style>
    <style id="__jsx-3491436448">
        .filter {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            margin: 12px;
            border-radius: 4px;
        }

        .filter .filter-value {
            position: relative;
        }

        .filter .filter-value .select-container {
            position: relative;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            padding: 0px 12px;
            font-size: 0.875rem;
            border-radius: 4px;
            width: 100%;
            border-radius: 8px;
            border: 1px solid;
            border-color: #DCDFEC;
            cursor: pointer;
            min-height: 46px;
        }

        .filter .filter-value .select-container .down-arrow-wrapper {
            margin-right: 12px;
            -webkit-transition: -webkit-transform 0.3s ease-in-out;
            -webkit-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }

        .filter .filter-value .select-container .down-arrow-wrapper .down-arrow {
            width: 8px;
            height: 8px;
        }

        .filter .filter-value .select-container .down-arrow-wrapper.open {
            -webkit-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
            position: relative;
        }

        .filter .filter-value .select-container .selected-values {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            max-width: 380px;
            overflow: hidden;
        }

        .filter .filter-value .select-container .selected-values .ellipsis {
            font-size: 0.875rem;
            padding: 8px;
            background-color: #cce5ff;
            color: #333333;
            border-radius: 8px;
            margin: 8px 8px 8px 0;
        }

        .filter .filter-value .select-container .selected-values .filter-selected {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            padding: 8px 8px;
            background-color: #cce5ff;
            color: #333333;
            border-radius: 8px;
            font-size: 0.875rem;
            margin: 8px 8px 8px 0;
            max-height: 35px;
        }

        .filter .filter-value .select-container .selected-values .filter-selected .close-button {
            margin-left: 8px;
            cursor: pointer;
        }

        .filter .filter-value .select-container .selected-values .filter-selected .close-button svg {
            height: 8px;
        }

        .filter .filter-value .select-container .selected-values .filter-selected:hover .close-button {
            color: black;
        }

        .filter .filter-value .select-container .placeholder {
            color: #323338;
            padding-left: 8px;
        }

        .filter .filter-value .select-container .placeholder.open {
            font-weight: 500;
        }

        .filter .filter-value .select-container .arrow-down {
            width: 0;
            height: 0;
            border-left: 8px solid transparent;
            border-right: 8px solid transparent;
            border-top: 8px solid black;
            margin-right: 8px;
        }

        .filter .filter-value .select-container:hover {
            background-color: #f7f7f7;
        }

        .filter .filter-value .select-container.open {
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .filter .filter-value .options-container {
            top: 100%;
            left: 0;
            z-index: 1;
            display: none;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            background-color: #ffffff;
            border-top: none;
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            overflow-y: auto;
            max-height: 300px;
            width: 100%;
            position: relative;
            padding-top: 12px;
            padding-bottom: 12px;
        }

        .filter .filter-value .options-container .option {
            padding: 8px 12px;
            font-size: 0.875rem;
            line-height: 20px;
            cursor: pointer;
            text-align: left;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            margin: 4px 8px;
            border-radius: 8px;
        }

        .filter .filter-value .options-container .option:hover {
            background-color: #f7f7f7;
        }

        .filter .filter-value .options-container .option.selected {
            background-color: #f7f7f7;
        }

        .filter .filter-value .options-container .option .icon {
            padding-right: 4px;
        }

        .filter .filter-value .options-container .option .icon .asset-inner .picture-component img {
            max-height: 20px;
        }

        .filter .filter-value .options-container .option .checkbox {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            width: 20px;
            height: 20px;
            border: 1px solid;
            border-color: #DCDFEC;
            border-radius: 3px;
            margin-right: 8px;
            cursor: pointer;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .filter .filter-value .options-container .option .checkbox .v-sign {
            padding: 2px;
            display: none;
        }

        .filter .filter-value .options-container .option .checkbox .v-sign svg {
            max-height: 9px;
        }

        .filter .filter-value .options-container .option .checkbox.selected {
            background-color: #0073ea;
            text-align: center;
        }

        .filter .filter-value .options-container .option .checkbox.selected .v-sign {
            display: unset;
        }

        .filter .filter-value .options-container::before {
            content: "";
            position: absolute;
            top: 0;
            left: 20px;
            width: calc(100% - 40px);
            height: 0.5px;
            background-color: #DCDFEC;
        }

        .filter .filter-value.open .options-container {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .filter .filter-value.open .select-container {
            border: none;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .filter.open {
            box-shadow: 0 2px 6px 0 rgba(29, 140, 242, 0.16);
            border-color: #DCDFEC;
        }
    </style>
    <style id="__jsx-3611417090">
        .filters-container {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            width: 30%;
            padding: 24px;
            padding-top: 0;
            margin-right: 16px;
            min-width: 340px;
            padding-bottom: 108px;
            box-shadow: 32px 12px 40px -32px rgba(29, 140, 242, 0.16);
            height: calc(100% - 24px);
        }

        .filters-container .filters-content {
            position: -webkit-sticky;
            position: sticky;
            top: 80px;
        }

        .filters-container .filters-content .filters-title {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-align-items: flex-start;
            -webkit-box-align: flex-start;
            -ms-flex-align: flex-start;
            align-items: flex-start;
            margin-bottom: 32px;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            margin-left: 16px;
            margin-right: 18px;
        }

        .filters-container .filters-content .text-filter-container {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            border-radius: 8px;
            border: 1px solid;
            border-color: #DCDFEC;
            margin: 42px 12px;
            position: relative;
        }

        .filters-container .filters-content .text-filter-container .search-icon-wrapper {
            position: absolute;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            padding-left: 16px;
        }

        .filters-container .filters-content .text-filter-container input {
            -webkit-flex: 1;
            -ms-flex: 1;
            flex: 1;
            padding: 12px;
            border-radius: 4px;
            font-size: 0.875rem;
            border-radius: 8px;
            border: none;
            color: #323338;
            padding-left: 42px;
            width: 100%;
        }

        .filters-container .filters-content .text-filter-container input:focus {
            outline: none;
        }

        .filters-container .filters-content .filters-title h2 {
            margin: 0;
            font-size: 1.125rem;
            font-weight: 500;
            line-height: 18px;
        }

        .filters-container .filters-content .clear-all {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            font-size: 0.875rem;
            color: #333333;
            cursor: pointer;
            -webkit-align-self: end;
            -ms-flex-item-align: end;
            align-self: end;
        }

        .filters-container .filters-content .clear-all:hover {
            color: #6c63ff;
        }
    </style>
    <style id="__jsx-2383196132">
        .tag {
            font-size: 12px;
            border-radius: 4px;
            margin-bottom: 10px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            margin-right: 8px;
            background-color: transparent;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            padding: 6px 12px;
        }

        .tag .icon {
            padding-right: 4px;
        }

        .tag .icon .asset-inner .picture-component img {
            width: 16px;
        }

        .release-status {
            -webkit-align-self: left;
            -ms-flex-item-align: left;
            align-self: left;
            background-color: #eafbe7;
            padding: 4px;
            border-radius: 4px;
        }

        .release-status .tag-dot {
            width: 6px;
            height: 6px;
            border-radius: 50%;
            margin-right: 8px;
            -webkit-align-self: center;
            -ms-flex-item-align: center;
            align-self: center;
            background-color: #328048;
        }
    </style>
    <style id="__jsx-3821249285">
        .secondary-button-wrapper:hover {
            outline: 0;
        }

        .play-wrapper {
            padding-right: 16px;
        }

        .play-wrapper svg {
            display: block;
        }

        .play-wrapper svg circle {
            stroke: currentColor;
        }

        .play-wrapper svg path {
            fill: currentColor;
        }

        .play-wrapper.regular-button {
            padding-right: 4px;
        }
    </style>
    <style id="__jsx-591613799">
        .square-list-container {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-align-items: flex-start;
            -webkit-box-align: flex-start;
            -ms-flex-align: flex-start;
            align-items: flex-start;
            max-width: 1440px;
            width: 100%;
        }

        .square-list-container .square-list {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            width: 100%;
            padding: 42px 24px 0;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .square-list-container .square-wrapper {
            margin-left: 2%;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
        }

        .square-list-container .square-wrapper.placeholder {
            height: 350px;
        }

        .square-list-container .desktop-date-title {
            width: 100%;
            margin-left: 2%;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
        }

        .square-list-container .desktop-date-title .date-content {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            font-size: 14px;
            line-height: 30px;
            padding: 24px 0;
        }

        .square-list-container .desktop-date-title .date-content .popup-button-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
        }

        .square-list-container .square {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-align-items: flex-start;
            -webkit-box-align: flex-start;
            -ms-flex-align: flex-start;
            align-items: flex-start;
            width: 96%;
            margin: 16px 0;
            border-radius: 8px;
            box-shadow: 0 0 4px rgba(29, 140, 242, 0.16);
            position: relative;
        }

        .square-list-container .square:hover {
            box-shadow: 0 0 16px rgba(29, 140, 242, 0.16);
        }

        .square-list-container .line-dot-container {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            margin-right: 44px;
            position: relative;
        }

        .square-list-container .line-dot-container .vertical-line {
            width: 0.5px;
            height: 100%;
            background-color: #c4c4c4;
        }

        .square-list-container .line-dot-container .vertical-line.page-first-item {
            margin-top: 28px;
        }

        .square-list-container .line-dot-container .dot {
            position: absolute;
            top: 34px;
            width: 12px;
            height: 12px;
            border-radius: 50%;
            background-color: #c4c4c4;
        }

        .square-list-container .line-dot-container .date-dot {
            position: absolute;
            top: 32px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            background-color: white;
        }

        .square-list-container .line-dot-container .date-dot .inner-dot {
            width: 12px;
            height: 12px;
            border-radius: 50%;
            background-color: #6161FF;
            z-index: 100;
        }

        .square-list-container .line-dot-container .date-dot .outer-ring {
            position: absolute;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            border: 2px solid;
            border-color: #6161FF;
            background-color: #ffffff;
        }

        .square-list-container .square .content-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            width: 65%;
            padding: 18px;
            padding-bottom: 0;
        }

        .square-list-container .square .image-wrapper {
            width: 30%;
        }

        .square-list-container .square .tags-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }

        .square-list-container .square .tags-wrapper .left-tags-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            margin-bottom: 8px;
            gap: 4px;
        }

        .square-list-container .square .name {
            font-size: 1.25rem;
            font-weight: 600;
            margin-bottom: 8px;
            margin-top: 20px;
            text-align: left;
            line-height: 26px;
        }

        .square-list-container .square .description {
            font-size: 0.875rem;
            margin-bottom: 18px;
            text-align: left;
            line-height: 22px;
        }

        .square-list-container .pagination-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            width: 100%;
            margin-left: 2%;
        }

        .square-list-container .pagination {
            margin-top: 16px;
            margin-bottom: 32px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            width: 100%;
            padding-bottom: 28px;
        }

        .square-list-container .pagination .pagination-button {
            margin: 0 8px;
            cursor: pointer;
            -webkit-text-decoration: none;
            text-decoration: none;
            font-size: 1.125rem;
            line-height: 22px;
            font-weight: 400;
        }

        .square-list-container .pagination span:not(.pagination-button) {
            font-size: 1.125rem;
            line-height: 22px;
            font-weight: 400;
        }

        .square-list-container .square-list .empty-state {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .square-list-container .square-list .empty-state .empty-state {
            width: 96%;
            padding: 16px 0 40px;
            margin-left: 2%;
            text-align: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            gap: 8px;
        }

        .square-list-container .square-list .empty-state .empty-state .empty-message-title {
            font-size: 1.25rem;
            font-weight: 600;
        }

        .square-list-container .square-list .empty-state .empty-state .empty-message-subtitle {
            font-size: 1rem;
            font-style: italic;
        }

        .square-list-container .square-list {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            -ms-flex-pack: start;
            justify-content: flex-start;
        }

        .square-list-container .square-list.empty-list {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .square-list-container .square-list .square-list-title {
            margin-left: calc(2% - 10px);
            margin-bottom: 16px;
        }

        .square-list-container .square-list .square-list-title .core-title {
            font-weight: 200;
            font-size: 1.5rem;
        }

        .square-list-container .square .image-wrapper.mobile-image-wrapper {
            display: none;
        }

        .square-list-container .square .image-wrapper.desktop-image-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            height: 100%;
            border-radius: 8px;
        }

        .square-list-container .square .image-wrapper.desktop-image-wrapper .asset-inner .picture-component {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            height: 100%;
        }

        .square-list-container .square .image-wrapper.desktop-image-wrapper .asset-inner .picture-component img {
            border-top-right-radius: 8px;
            border-bottom-right-radius: 8px;
        }

        .square-list-container .square .card-top-part-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-align-items: left;
            -webkit-box-align: left;
            -ms-flex-align: left;
            align-items: left;
        }

        .card-top-part-wrapper .release-type-name {
            font-size: 0.875rem;
        }

        .card-top-part-wrapper .separator-dot {
            display: inline-block;
            margin: 0 5px;
            vertical-align: middle;
            font-size: 1rem;
        }

        .square-list-container .square .content-wrapper .card-top-part-wrapper .release-type-icon .icon {
            width: 14px;
            margin-right: 4px;
        }

        .card-top-part-wrapper .full-date-title {
            font-size: 0.875rem;
            text-align: left;
            color: #333333;
        }

        .square-list-container .square .read-more {
            padding-bottom: 16px;
        }

        @media (max-width: 768px) {
            .square-list-container {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column;
            }

            .square-list-container .filters-container {
                display: none;
            }

            .square-list-container .square-list {
                padding-top: 0;
                gap: 24px;
                -webkit-box-pack: unset;
                -webkit-justify-content: unset;
                -ms-flex-pack: unset;
                justify-content: unset;
            }

            .square-list-container .square-list .square-list-title {
                display: none;
            }

            .square-list-container .square-list .content-wrapper {
                width: 100%;
                padding: 0;
            }

            .square-list-container .square-list .content-wrapper .name {
                margin-bottom: 18px;
                margin-top: 0;
            }

            .square-list-container .square .image-wrapper.mobile-image-wrapper {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                width: 100%;
            }

            .square-list-container .square .image-wrapper.desktop-image-wrapper {
                display: none;
            }

            .square-list-container .square .tags-wrapper {
                -webkit-flex-wrap: wrap;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
            }

            .square-list-container .square .tags-wrapper .left-tags-wrapper {
                -webkit-flex-wrap: wrap;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-order: 1;
                -ms-flex-order: 1;
                order: 1;
            }

            .square-list-container .square-wrapper .mobile-date-title {
                font-size: 0.875rem;
                text-align: left;
                color: #20303C;
            }

            .square-list-container .line-dot-container {
                display: none;
            }

            .square-list-container .square-wrapper {
                display: unset;
            }

            .square-list-container .square {
                border-bottom: 1px solid;
                border-color: #DCDFEC;
                border-radius: unset;
                box-shadow: unset;
                position: unset;
                padding: 32px 0;
                margin: unset;
            }

            .square-list-container .square:hover {
                box-shadow: unset;
            }

            .square-list-container .pagination-wrapper .line-dot-container {
                display: none;
            }

            .square-list-container .square .description {
                margin-bottom: 16px;
            }
        }
    </style>
    <style id="__jsx-1675809481">
        .link.jsx-1675809481 {
            line-height: 22px;
        }

        .link.jsx-1675809481:not(:last-child) {
            margin-bottom: 16px;
        }

        .link.jsx-1675809481:hover {
            color: #5034FF;
        }

        .link.jsx-1675809481 a.jsx-1675809481 {
            -webkit-text-decoration: none;
            text-decoration: none;
            color: inherit;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-column-gap: 8px;
            column-gap: 8px;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .link.jsx-1675809481 a.jsx-1675809481 .picture-component {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .link.jsx-1675809481 a.jsx-1675809481 .picture-component img {
            height: 18px;
        }
    </style>
    <style id="__jsx-1727271368">
        .footer-category-component.jsx-1727271368 .footer-category-image-and-title-warpper.jsx-1727271368 .monday-logo-wrapper.jsx-1727271368 {
            margin-bottom: 14px;
        }

        .footer-category-component.jsx-1727271368 .footer-category-image-and-title-warpper.jsx-1727271368 .monday-logo-wrapper.jsx-1727271368 .footer-monday-logo-image {
            height: 32px;
        }

        .footer-category-component.jsx-1727271368 .footer-category-image-and-title-warpper.jsx-1727271368 .title.jsx-1727271368 {
            margin-top: 10px;
            margin-bottom: 24px;
            font-size: 0.875rem;
            font-weight: 500;
        }
    </style>
    <style id="__jsx-2921979010">
        .language-picker-item-component {
            height: 32px;
            width: 104px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            border-radius: 4px;
            padding: 0px 8px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-transition: background-color 200ms ease;
            transition: background-color 200ms ease;
        }

        .language-picker-item-component:hover {
            background-color: #F5F6F8;
        }

        .language-picker-item-component.language-selected {
            background-color: #F0F3FF;
        }

        .language-picker-item-component.language-selected .language-picker-item-label {
            color: #5034FF;
        }

        .language-picker-item-component .language-picker-item-label {
            -webkit-box-flex: 1;
            -webkit-flex-grow: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            line-height: 32px;
            font-size: 0.8125rem;
            color: #535768;
        }
    </style>
    <style id="__jsx-1748029790">
        .language-picker-dialog-component {
            border-radius: 4px;
            background-color: white;
            box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.1);
            padding: 4px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }

        .language-picker-dialog-component .language-picker-item-component-wrapper {
            margin: 4px;
        }

        @media (max-width: 450px) {
            .language-picker-dialog-component {
                -webkit-flex-direction: row;
                -ms-flex-direction: row;
                flex-direction: row;
                overflow: auto;
                width: 100% !important;
                max-height: 186px;
            }
        }
    </style>
    <style id="__jsx-2449401012">
        .language-picker-component {
            position: relative;
        }

        .language-picker-component:hover .earth-icon path,
        .language-picker-component.is-open .earth-icon path {
            stroke: #5034FF;
        }

        .language-picker-component:hover .selected-language .language-label,
        .language-picker-component.is-open .selected-language .language-label {
            color: #5034FF;
        }

        .language-picker-component:hover .arrow-down-icon path,
        .language-picker-component.is-open .arrow-down-icon path {
            fill: #5034FF;
        }

        .language-picker-component.is-open .selected-language .arrow-down-icon {
            -webkit-transform: translateY(1px) rotate(180deg);
            -ms-transform: translateY(1px) rotate(180deg);
            transform: translateY(1px) rotate(180deg);
        }

        .language-picker-component.is-open .language-picker-dialog-component-wrapper {
            opacity: 1;
            visibility: visible;
        }

        .language-picker-component .selected-language {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            height: 32px;
            line-height: 32px;
        }

        .language-picker-component .selected-language .earth-icon path {
            -webkit-transition: stroke 150ms ease;
            transition: stroke 150ms ease;
            color: #535768;
        }

        .language-picker-component .selected-language .language-label {
            font-size: 0.875rem;
            margin: 0px 8px;
            max-width: 104px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            -webkit-transition: color 150ms ease;
            transition: color 150ms ease;
            color: #535768;
        }

        .language-picker-component .selected-language .arrow-down-icon {
            height: 10px;
            width: 10px;
            -webkit-transform: translateY(1px);
            -ms-transform: translateY(1px);
            transform: translateY(1px);
            -webkit-transition: -webkit-transform 200ms ease;
            -webkit-transition: transform 200ms ease;
            transition: transform 200ms ease;
        }

        .language-picker-component .selected-language .arrow-down-icon path {
            fill: #535768;
            -webkit-transition: fill 150ms ease;
            transition: fill 150ms ease;
        }

        .language-picker-component .language-picker-dialog-component-wrapper {
            position: absolute;
            top: 40px;
            opacity: 0;
            visibility: hidden;
            -webkit-transition: opacity 200ms ease;
            transition: opacity 200ms ease;
            z-index: 1032;
        }

        .language-picker-component .language-picker-dialog-component-wrapper.left {
            left: 0;
        }

        .language-picker-component .language-picker-dialog-component-wrapper.right {
            right: 0;
        }

        .language-picker-component .language-picker-dialog-component-wrapper.top {
            top: calc((-40 * var(--number-of-lang-rows)) * 1px - 16px);
        }

        .language-picker-component .language-picker-dialog-component-wrapper.bottom .language-picker-dialog-component {
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
        }

        @media (max-width: 450px) {
            .language-picker-component .language-picker-dialog-component-wrapper.top {
                top: -200px;
            }
        }
    </style>
    <style id="__jsx-3089181165">
        .security-logos-component {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .security-logos-component .iso-logo-img-container,
        .security-logos-component .gdpr-logo-img-container,
        .security-logos-component .hipaa-logo-img-container,
        .security-logos-component .soc-logo-img-container {
            margin-right: 8px;
            cursor: pointer;
        }

        .security-logos-component .iso-logo-img-container img,
        .security-logos-component .gdpr-logo-img-container img,
        .security-logos-component .hipaa-logo-img-container img,
        .security-logos-component .soc-logo-img-container img {
            height: 32px;
        }

        .security-logos-component .iso-logo-img-container:hover img,
        .security-logos-component .gdpr-logo-img-container:hover img,
        .security-logos-component .hipaa-logo-img-container:hover img,
        .security-logos-component .soc-logo-img-container:hover img {
            opacity: 0.7;
        }
    </style>
    <style id="__jsx-3692625068">
        .social-media-icon svg {
            height: 24px;
            width: auto;
        }

        .social-media-icon:hover svg path {
            fill: #5034FF;
        }
    </style>
    <style id="__jsx-4139997252">
        .palette-icons-container.jsx-4139997252 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .palette-icons-container.jsx-4139997252 .icons-wrapper.jsx-4139997252 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            gap: 16px;
        }

        .palette-icons-container.jsx-4139997252 .single-app-icon-wrapper.jsx-4139997252 {
            margin-right: 8px;
        }

        @media (max-width: 768px) {
            .palette-icons-container.jsx-4139997252 {
                margin: 0 auto;
                padding: 0;
                -webkit-flex-wrap: wrap;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                margin-top: 16px;
            }

            .palette-icons-container.jsx-4139997252 .icons-wrapper.jsx-4139997252:first-child {
                margin-right: auto;
            }

            .palette-icons-container.jsx-4139997252 .icons-wrapper.jsx-4139997252 {
                width: 200px;
                margin: 0 auto;
                margin-bottom: 8px;
            }
        }
    </style>
    <style id="__jsx-4225634998">
        .footer-bottom-bar-link-component {
            font-size: 0.8125rem;
            color: #535768;
            -webkit-transition: 100ms color ease;
            transition: 100ms color ease;
            -webkit-text-decoration: underline;
            text-decoration: underline;
        }

        .footer-bottom-bar-link-component:hover {
            color: #595ad4;
        }
    </style>
    <style id="__jsx-3477892871">
        .footer-bottom-bar-component.jsx-3477892871 {
            position: relative;
            font-size: 0.8125rem;
            color: #808080;
            width: 100%;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }

        .footer-bottom-bar-component.jsx-3477892871:before {
            display: block;
            content: "";
            height: 1px;
            width: calc(100% - 64px);
            background-color: #DCDFEC;
            position: relative;
            max-width: 1376px;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-both-sides-wrapper.jsx-3477892871 {
            max-width: 1440px;
            width: 100%;
            margin: 0 auto;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            padding: 40px 32px 48px;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            height: 180px;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            gap: 32px;
            height: 100%;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 .language-picker-and-security-logos-wrapper.jsx-3477892871 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            height: 100%;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 .language-picker-and-security-logos-wrapper.jsx-3477892871 .language-picker-wrapper.jsx-3477892871 {
            height: 40px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 .social-statement-and-links-wrapper.jsx-3477892871 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            height: 100%;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 .social-statement-and-links-wrapper.jsx-3477892871 .palette-icons-wrapper.jsx-3477892871 {
            height: 40px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 .social-statement-and-links-wrapper.jsx-3477892871 .statement-and-links-wrapper.jsx-3477892871 {
            height: 24px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-bottom: 6px;
            gap: 24px;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 .social-statement-and-links-wrapper.jsx-3477892871 .statement-and-links-wrapper.jsx-3477892871 .links-container.jsx-3477892871 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 .social-statement-and-links-wrapper.jsx-3477892871 .statement-and-links-wrapper.jsx-3477892871 .links-container.jsx-3477892871 .short-footer-link-component-wrapper.jsx-3477892871 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 .social-statement-and-links-wrapper.jsx-3477892871 .statement-and-links-wrapper.jsx-3477892871 .links-container.jsx-3477892871 .short-footer-link-component-wrapper.jsx-3477892871:not(:last-of-type):after {
            content: " | ";
            margin: 0 8px;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-left-side.jsx-3477892871 .social-statement-and-links-wrapper.jsx-3477892871 .statement-and-links-wrapper.jsx-3477892871 .all-rights-reserved-component {
            padding-bottom: 2px;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-right-side.jsx-3477892871 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            height: 100%;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-right-side.jsx-3477892871 .app-store-icons.jsx-3477892871 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            gap: 16px;
            height: 40px;
        }

        .footer-bottom-bar-component.jsx-3477892871 .footer-bottom-bar-right-side.jsx-3477892871 .accessibility-statement-link-wrapper.jsx-3477892871 {
            height: 24px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-bottom: 6px;
        }

        @media (max-width: 991px) and (min-width:768px) {
            .footer-bottom-bar-component.jsx-3477892871 .items-container.jsx-3477892871 {
                padding: 0 54px;
            }
        }
    </style>
    <style id="__jsx-164348419">
        .all-rights-reserved-component .details {
            font-size: 0.8125rem;
            line-height: 2;
            height: 24px;
            color: #535768;
        }

        @media (max-width: 768px) {
            .all-rights-reserved-component {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                margin-left: 24px;
            }

            .all-rights-reserved-component .icons-wrapper {
                margin-left: 48px;
            }
        }
    </style>
    <style id="__jsx-804200692">
        .accessibilty-statement-link-component {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            font-size: 0.75rem;
            cursor: pointer;
        }

        .accessibilty-statement-link-component .accessibility-icon-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }

        .accessibilty-statement-link-component .accessibility-icon-wrapper .accessibility-icon circle,
        .accessibilty-statement-link-component .accessibility-icon-wrapper .accessibility-icon path {
            color: #535768;
            -webkit-transition: stroke 150ms ease;
            transition: stroke 150ms ease;
        }

        .accessibilty-statement-link-component .accessibility-link-wrapper {
            padding-left: 8px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .accessibilty-statement-link-component .accessibility-link-wrapper .accessibility-link {
            -webkit-transition: 150ms color ease;
            transition: 150ms color ease;
            color: #535768;
        }

        .accessibilty-statement-link-component:hover .accessibility-icon-wrapper .accessibility-icon path,
        .accessibilty-statement-link-component:hover .accessibility-icon-wrapper .accessibility-icon circle {
            stroke: #595ad4;
        }

        .accessibilty-statement-link-component:hover .accessibility-link-wrapper .accessibility-link {
            color: #595ad4;
        }
    </style>
    <style id="__jsx-481973735">
        .short-footer-link-component {
            -webkit-text-decoration: none;
            text-decoration: none;
            cursor: default;
            width: -webkit-fit-content;
            width: -moz-fit-content;
            width: fit-content;
            color: #333333;
            line-height: 12px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            gap: 4px;
            -webkit-transition: 100ms color ease;
            transition: 100ms color ease;
        }

        .short-footer-link-component .link-icon {
            height: 12px;
            width: 12px;
        }

        .short-footer-link-component.main-title {
            font-weight: 600;
            font-size: 0.875rem;
        }

        .short-footer-link-component.main-title.clickable {
            -webkit-text-decoration: underline;
            text-decoration: underline;
            cursor: pointer;
        }

        .short-footer-link-component.secondary-title {
            font-weight: 600;
            color: #676879;
            font-size: 0.75rem;
        }

        .short-footer-link-component.link {
            font-weight: 400;
            font-size: 0.75rem;
            cursor: pointer;
        }

        .short-footer-link-component.link:hover {
            color: #595ad4;
        }
    </style>
    <style id="__jsx-4178579150">
        .short-footer-component {
            color: #333333;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding: 48px 80px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .short-footer-component .short-footer-header {
            width: 100%;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            margin-bottom: 48px;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
        }

        .short-footer-component .short-footer-header .language-picker-wrapper-component {
            width: 120px;
        }

        .short-footer-component .short-footer-header .language-picker-wrapper-component .language-picker-component .language-picker-dialog-component-wrapper {
            margin: 0;
        }

        .short-footer-component .short-footer-links {
            width: 100%;
            -webkit-column-count: 4;
            column-count: 4;
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }

        .short-footer-component .short-footer-links .link-wrapper {
            -webkit-flex-basis: 100%;
            -ms-flex-preferred-size: 100%;
            flex-basis: 100%;
            -webkit-break-inside: avoid;
            break-inside: avoid;
            margin: 0 0 20px;
        }

        .short-footer-component .short-footer-links .link-wrapper.main:not(:first-child) {
            margin-top: 28px;
        }

        .short-footer-component .short-footer-links .link-wrapper.main:nth-child(14n),
        .short-footer-component .short-footer-links .link-wrapper.main:nth-child(23n),
        .short-footer-component .short-footer-links .link-wrapper.main:nth-child(35n) {
            -webkit-break-before: column;
            break-before: column;
            margin-top: 0;
        }

        .short-footer-component .bottom {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            margin-top: 32px;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            gap: 12px;
        }

        .short-footer-component .bottom .terms {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            gap: 12px;
        }

        .short-footer-component .bottom .terms .accessibility-statement-link-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .short-footer-component .bottom .footer-link {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-text-decoration: underline;
            text-decoration: underline;
        }

        .short-footer-component .bottom .footer-link a {
            -webkit-transition: 150ms color ease;
            transition: 150ms color ease;
            color: #535768;
            font-size: 0.75rem;
        }

        .short-footer-component .monday-logo-wrapper .footer-monday-logo-image {
            height: 32px;
        }

        .short-footer-component .contact-info-wrapper {
            font-size: 0.8125rem;
            text-align: center;
        }

        @media (max-width: 768px) {
            .short-footer-component {
                -webkit-align-items: initial;
                -webkit-box-align: initial;
                -ms-flex-align: initial;
                align-items: initial;
                padding: 32px;
            }

            .short-footer-component .short-footer-links {
                -webkit-column-count: 2;
                column-count: 2;
                -webkit-flex-wrap: wrap;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
            }

            .short-footer-component .short-footer-links .link-wrapper.main:nth-child(14n),
            .short-footer-component .short-footer-links .link-wrapper.main:nth-child(35n) {
                -webkit-break-before: auto;
                break-before: auto;
                margin-top: 28px;
            }

            .short-footer-component .bottom {
                -webkit-align-items: initial;
                -webkit-box-align: initial;
                -ms-flex-align: initial;
                align-items: initial;
            }

            .short-footer-component .bottom .contact-info-wrapper {
                text-align: left;
                width: 100%;
                line-height: 24px;
            }

            .short-footer-component .bottom .contact-info-wrapper .address-info {
                margin-top: 0;
            }

            .short-footer-component .bottom .social-proof-wrapper {
                width: 100%;
            }

            .short-footer-component .bottom .social-proof-wrapper .palette-icons-container {
                margin-top: 0;
            }

            .short-footer-component .bottom .social-proof-wrapper .icons-wrapper {
                margin: 0 !important;
            }
        }

        @media (max-width: 768px) and (max-width:451px) {
            .terms {
                -webkit-flex-wrap: wrap;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
            }

            .terms .accessibility-statement-link-wrapper {
                -webkit-flex-basis: 100%;
                -ms-flex-preferred-size: 100%;
                flex-basis: 100%;
            }
        }

        @media (max-width: 768px) and (max-width:768px) and (min-width:451px) {
            .short-footer-component {
                -webkit-align-items: center;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
            }

            .short-footer-component .short-footer-links {
                -webkit-column-count: 3;
                column-count: 3;
                -webkit-flex-wrap: wrap;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
            }

            .short-footer-component .short-footer-links .link-wrapper.main:nth-child(35n) {
                -webkit-break-before: column;
                break-before: column;
                margin-top: 0;
            }

            .short-footer-component .bottom {
                width: 100%;
                -webkit-align-items: center;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-box-pack: center;
                -webkit-justify-content: center;
                -ms-flex-pack: center;
                justify-content: center;
            }

            .short-footer-component .bottom .contact-info-wrapper {
                text-align: center;
            }

            .short-footer-component .bottom .social-proof-wrapper {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-align-items: center;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-box-pack: center;
                -webkit-justify-content: center;
                -ms-flex-pack: center;
                justify-content: center;
            }

            .short-footer-component .bottom .social-proof-wrapper .icons-wrapper {
                width: -webkit-fit-content;
                width: -moz-fit-content;
                width: fit-content;
            }
        }
    </style>
    <style id="__jsx-463568449">
        .footer-desktop-wrapper {
            display: block;
        }

        .footer-mobile-wrapper {
            display: none;
        }

        @media (max-width: 1199px) {
            .footer-desktop-wrapper {
                display: none;
            }

            .footer-mobile-wrapper {
                display: block;
            }
        }

        .footer-container {
            width: 100%;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            overflow: hidden;
            color: #535768;
            background-color: #ffffff;
        }

        .footer-container .footer-content-container {
            max-width: 1440px;
            width: 100%;
            padding: 80px 32px 0 32px;
            font-size: 0.8125rem;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            position: relative;
        }

        .footer-container .footer-content-container .footer-content {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            width: 1440px;
        }

        .footer-container .footer-content-container .footer-content .all-right-reserved-container {
            -webkit-box-flex: 1;
            -webkit-flex-grow: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            margin-right: 120px;
        }

        .footer-container .footer-content-container .footer-content .categories-container {
            -webkit-box-flex: 1;
            -webkit-flex-grow: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-align-items: flex-start;
            -webkit-box-align: flex-start;
            -ms-flex-align: flex-start;
            align-items: flex-start;
        }

        .footer-container .footer-content-container .footer-content .categories-container .categories-colmmn-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            gap: 38px;
        }

        .footer-container .footer-content-container .footer-content .categories-container .category-container {
            -webkit-flex: 1 0 auto;
            -ms-flex: 1 0 auto;
            flex: 1 0 auto;
            margin-bottom: 48px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .footer-container .footer-content-container .footer-content .categories-container .category-container:last-child {
            -webkit-box-flex: 0;
            -webkit-flex-grow: 0;
            -ms-flex-positive: 0;
            flex-grow: 0;
        }

        .footer-container .footer-content-container .footer-content .categories-container .category-container:not(:last-child) {
            margin-right: 16px;
        }

        .footer-container:before {
            display: block;
            content: "";
            height: 1px;
            width: 100%;
            background-color: #DCDFEC;
            position: relative;
        }

        @media (max-width: 768px) {
            .footer-container .footer-content-container {
                padding: 40px 40px 0 40px;
            }

            .footer-container .footer-content-container .footer-content {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column;
                padding-left: 24px;
            }

            .footer-container .footer-content-container .footer-content .all-right-reserved-container {
                width: 100%;
            }

            .footer-container .footer-content-container .footer-content .categories-container .category-container {
                width: 130px;
                -webkit-align-items: center;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                margin-right: 12px;
            }

            .footer-container .footer-content-container .footer-content .categories-container .category-container .footer-category-component {
                width: 130px;
            }

            .footer-container .footer-content-container .footer-content .categories-container .security-logos-container img {
                width: 34px !important;
                height: 34px !important;
            }
        }

        @media (max-width: 360px) {
            .footer-container .footer-content-container {
                padding: 40px 16px 0 16px;
            }
        }

        @media (max-width: 1440px) {
            .footer-container .footer-content-container .footer-content {
                padding: 0;
            }
        }
    </style>
    <style id="__jsx-3664843607">
        .whats-new-page-component .whats-new-page-content {
            font-size: 54px;
            text-align: center;
        }

        .whats-new-page-component .whats-new-page-content .items-list-wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            margin-top: 40px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }
    </style>
    <style id="__jsx-1726528970">
        .whats-new-mobile-first-fold {
            display: grid;
            grid-template-rows: 1fr;
            grid-template-columns: 1fr;
        }

        .whats-new-mobile-first-fold .asset-wrapper {
            grid-row: 1 / 2;
            grid-column: 1 / 2;
            position: relative;
        }

        .whats-new-mobile-first-fold .asset-wrapper .dark-overlay {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0, 0, 0, 0.7);
            z-index: 9;
        }

        .whats-new-mobile-first-fold .asset-wrapper .asset-inner {
            height: 100%;
        }

        .whats-new-mobile-first-fold .asset-wrapper .asset-inner .picture-component {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            height: 100%;
        }

        .whats-new-mobile-first-fold .content-wrapper {
            grid-row: 1 / 2;
            grid-column: 1 / 2;
            z-index: 100;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-align-items: flex-start;
            -webkit-box-align: flex-start;
            -ms-flex-align: flex-start;
            align-items: flex-start;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            padding: 40px 24px;
            text-align: left;
        }

        .whats-new-mobile-first-fold .content-wrapper .core-title {
            font-weight: 700;
            margin-bottom: 16px;
            font-size: 2.25rem;
        }

        .whats-new-mobile-first-fold .content-wrapper .paragraph-body {
            font-size: 1rem;
            font-weight: 500;
            line-height: 28px;
        }
    </style>
    <style id="__jsx-3421323500">
        .whats-new-mobile-second-fold {
            padding: 0 0 48px 12px;
        }

        .whats-new-mobile-second-fold .title-wrapper {
            margin: 40px 10px 20px;
        }

        .whats-new-mobile-second-fold .title-wrapper .core-title {
            font-size: 1.75rem;
            font-weight: 600;
        }

        .whats-new-mobile-second-fold .carousel-wrapper .carousel .slick-slider .slick-dots li button {
            background-color: #ffffff;
            border: 1px solid;
            border-color: #323338;
        }

        .whats-new-mobile-second-fold .carousel-wrapper .carousel .slick-slider .slick-dots li.slick-active button {
            background-color: #323338;
        }

        .whats-new-mobile-second-fold .carousel-wrapper .carousel .slick-slider.focus .slick-list .slick-track .slick-slide .carousel-item {
            opacity: 1;
        }
    </style>
    <style id="__jsx-491659823">
        .whats-new-mobile-page-component .items-list-title-wrapper {
            margin: 44px 24px 0;
        }

        .whats-new-mobile-page-component .items-list-title-wrapper .core-title {
            font-size: 1.5rem;
            font-weight: 500;
        }

        .whats-new-mobile-page-component .filter-button-wrapper {
            box-shadow: 0px 4px 6px rgba(29, 140, 242, 0.1);
            margin-bottom: 24px;
            position: -webkit-sticky;
            position: sticky;
            top: 64px;
            background-color: #ffffff;
            z-index: 100;
        }

        .whats-new-mobile-page-component .filter-button-wrapper .filter-button {
            display: -webkit-inline-box;
            display: -webkit-inline-flex;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            background-color: #cce5ff;
            padding: 4px 8px;
            border-radius: 4px;
            font-size: 1.25rem;
            font-weight: bold;
            color: #333333;
            text-align: center;
            cursor: pointer;
            margin-bottom: 20px;
            margin: 24px;
        }

        .whats-new-mobile-page-component .filter-button-wrapper .filter-icon-wrapper {
            margin-right: 4px;
            margin-top: 4px;
        }

        .whats-new-mobile-page-component .filter-button-wrapper .filter-icon-wrapper svg {
            height: 28px;
        }

        .whats-new-mobile-page-component .filter-button-wrapper .filter-text {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            font-weight: 500;
        }

        .whats-new-mobile-page-component.no-scroll {
            overflow: hidden;
            position: fixed;
            height: 100%;
            width: 100%;
        }
    </style>
    <style id="__jsx-1985380231">
        .responsive-hoc-component.jsx-1985380231 .desktop-wrapper.jsx-1985380231 {
            display: block;
        }

        .responsive-hoc-component.jsx-1985380231 .mobile-wrapper.jsx-1985380231 {
            display: none;
        }

        @media (max-width: 768px) {
            .responsive-hoc-component.jsx-1985380231 .desktop-wrapper.jsx-1985380231 {
                display: none;
            }

            .responsive-hoc-component.jsx-1985380231 .mobile-wrapper.jsx-1985380231 {
                display: block;
            }
        }
    </style>
    <style id="__jsx-1326851841">
        .accessibility-menu-wrapper {
            position: relative;
            z-index: 2;
        }

        .template-content-wrapper {
            position: relative;
            z-index: 1;
        }

        .template-content-wrapper.with-background-color {
            background-color: white;
        }
    </style>



